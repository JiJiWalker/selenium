/*
* TODO Włączanie tylko najnowszych zadań po uruchomieniu instancji
* */

import java.io.File;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class ObslugaZamowieniaBPMApp {
  private WebDriver driver;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
//  private String baseUrl;
  private LocalDateTime ldt = LocalDateTime.now();

  @Before
  public void setUp() throws Exception {
    System.setProperty("webdriver.firefox.marionette", "C:\\geckodriver.exe");
    driver = new FirefoxDriver();
//    baseUrl = "https://dev-bpm85-003.ibpmpro.srv:9443/ProcessPortal/login.jsp";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testAplikacjaIbm() throws Exception {

//    strona logowania
    driver.get("https://dev-bpm85-003.ibpmpro.srv:9443/ProcessPortal/login.jsp");

//    logowanie
    driver.findElement(By.id("username")).sendKeys("");
    driver.findElement(By.id("password")).sendKeys("");
    driver.findElement(By.id("log_in")).click();

//    uruchamianie instancji zadania
    driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_LaunchableProcessList_0 > ul > li:nth-child(11) > a")).click();
    Thread.sleep(6000);
    driver.findElement(By.cssSelector("#overdue > div.dijitTitlePaneTitle.dijitOpen > div > span.dijitTitlePaneTextNode")).click();
    driver.findElement(By.id("com_ibm_bpm_social_widgets_task_list_SearchBar_0_input")).click();
    driver.findElement(By.id("com_ibm_bpm_social_widgets_task_list_SearchBar_0_input")).sendKeys("Przygotowanie zgłoszenia");
    driver.findElement(By.id("com_ibm_bpm_social_widgets_task_list_SearchBar_0_input")).sendKeys(Keys.ENTER);
    driver.findElement(By.linkText("Step: Przygotowanie zgłoszenia")).click();

    if (driver.getPageSource().contains("Zajmowanie zadania")) {
      driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_task_util_ClaimDialog_0 > div.bpm-task-dialog-claimButtonDiv > button")).click();
    }

//    Zadanie na później. Instancja najnowszego zadania. Poniżej przykładowe 3 selektory:
//    #com_ibm_bpm_social_widgets_task_list_TaskRow_19 > div:nth-child(1) > div:nth-child(2) > div:nth-child(3) > span:nth-child(1)
//    #com_ibm_bpm_social_widgets_task_list_TaskRow_18 > div:nth-child(1) > div:nth-child(2) > div:nth-child(3) > span:nth-child(1)
//    #com_ibm_bpm_social_widgets_task_list_TaskRow_20 > div:nth-child(1) > div:nth-child(2) > div:nth-child(3) > span:nth-child(1)


//    Od tego momentu trzeba obsłużyć iframe
    WebElement frame = driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe"));
    driver.switchTo().frame(frame);
    driver.findElement(By.id("input_div_1_1_1_1_1_1_2")).clear();
    driver.findElement(By.id("input_div_1_1_1_1_1_1_2")).sendKeys("123456789");
    driver.findElement(By.id("input_div_1_1_1_1_2")).clear();
    driver.findElement(By.id("input_div_1_1_1_1_2")).sendKeys("jgierko");
    driver.findElement(By.id("input_div_1_1_1_1_2")).sendKeys(Keys.ENTER);

//    Wybieranie wartości z rozwijanej listy

    new Select(driver.findElement(By.id("combo_div_2"))).selectByVisibleText("Usterka");
    driver.findElement(By.cssSelector("option[value=\"Usterka\"]")).click();

    new Select(driver.findElement(By.id("combo_div_3"))).selectByVisibleText("Serwis - C.H. Wola Park");
    driver.findElement(By.cssSelector("option[value=\"Serwis - C.H. Wola Park\"]")).click();

    new Select(driver.findElement(By.id("combo_div_4"))).selectByVisibleText("Reklamacyjny");

//    Rozwijanie listy pliku
    driver.findElement(By.cssSelector("#collapseHeader_div_6")).click();

//    Dodawanie pliku
    driver.findElement(By.cssSelector("input.blue:nth-child(2)")).click();
    Thread.sleep(2500);
    driver.findElement(By.cssSelector("#widget_cmis\\:name > div:nth-child(3) > input:nth-child(1)")).click();
    LocalDateTime ldt = LocalDateTime.now();
    driver.findElement(By.cssSelector("#widget_cmis\\:name > div:nth-child(3) > input:nth-child(1)")).sendKeys("Selenium test");

//    Dodawanie pliku
    Thread.sleep(2000);
//        Firefox selector
//        driver.findElement(By.cssSelector("#fileContent > td:nth-child(2) > input:nth-child(1)")).click();
    File file = new File("C:\\Users\\Jakub\\Desktop\\Selenium\\seletest.txt");
    String getFilePath = file.getAbsolutePath();
    driver.findElement(By.cssSelector("#fileContent > td:nth-child(2) > input[type=\"file\"]")).sendKeys(getFilePath);
    System.out.println("po");
    driver.findElement(By.cssSelector("input.blue:nth-child(7)")).click();
    Thread.sleep(2000);

//        Alert handler
    Alert alert = driver.switchTo().alert();
    String alertMessage = driver.switchTo().alert().getText();
    System.out.println(alertMessage);
    alert.accept();

//        Odświeważnie po dodaniu
    Thread.sleep(2000);
    driver.findElement(By.cssSelector("#div_6_1_1_1_1 > div > input:nth-child(3)")).click();
    System.out.println("Odświeżone");


//    Dodawanie komentarza
    driver.findElement(By.id("input_div_7_1_1")).clear();
    driver.findElement(By.id("input_div_7_1_1")).sendKeys("Komentarz");
    driver.findElement(By.cssSelector("button.btn.btn-default")).click();

//    Odświeżanie raz drugi
    driver.findElement(By.cssSelector("#div_6_1_1_1_1 > div > input:nth-child(3)")).click();

//    Zatwierdzenie
//    driver.findElement(By.cssSelector("#div_9_2_4_1_2 > div.buttonDiv > button.btn.btn-default")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
