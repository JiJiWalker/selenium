import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class KORUsterkaThirdStep {

    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {

        System.setProperty("webdriver.firefox.marionette", "C:\\geckodriver.exe");
        driver = new FirefoxDriver();
        baseUrl = "https://bpms-tst-app1.bpms.inea.ad:9443/ProcessPortal/login.jsp";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testKORTworzenieUsterki() throws Exception {

        driver.get(baseUrl);
        WebDriverWait wait = new WebDriverWait(driver, 10);

//    Logowanie
        WebElement userName;
        WebElement password;
        WebElement logIn;
        WebElement searchBar;
        WebElement clickCase;

        Thread.sleep(200);
        userName = driver.findElement(By.id("username"));
//    WebDriverWait wait = new WebDriverWait(driver, 10);
//    WebElement el = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
//
        userName.sendKeys("");
        password = driver.findElement(By.id("password"));
        password.sendKeys("");
        logIn = driver.findElement(By.id("log_in"));
        logIn.click();

//        Wyszukiwanie etapu
        searchBar = driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_task_list_SearchBar_0_input"));
        wait.until(ExpectedConditions.visibilityOf(searchBar)).sendKeys("Uzupełnienie szczegółów");
        searchBar.sendKeys(Keys.ENTER);
        searchBar.clear();

//        Klikanie etapu
        Thread.sleep(2000);
//        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#dueToday > div.dijitTitlePaneTitle.dijitOpen > div > span.dijitTitlePaneTextNode")));
        WebElement cc = driver.findElements(By.className("bpm-social-task-row-data-subject")).get(0);
        wait.until(ExpectedConditions.visibilityOf(cc));
        System.out.println(cc.getText());
        cc.click();

        System.out.println("Działa");

//        iframe
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)")));
        driver.switchTo().frame(driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)")));
        driver.manage().window().maximize();

//        Formularz
        WebElement smsNotification;
        WebElement contactPerson;
        WebElement applicationDescription;
        WebElement isAppointment;
        WebElement hboVisibility;

        List<WebElement> lwe = driver.findElements(By.className("dataTableSelectControl"));


//        for (int i = 0; i < lwe.size(); i++) {
//            System.out.println(lwe.get(i).getAttribute("id") + " [" + i + "]");
//        }

//        Sekcja klasyfikacja sprawy
        smsNotification = driver.findElement(By.cssSelector("#div_7 > div > div.controls > label > input[type=\"checkbox\"]"));
        if (smsNotification.isSelected()) {
            smsNotification.click();
        }

        contactPerson = driver.findElement(By.cssSelector("#input_div_6_1_1_1_1_1_1_3"));
        contactPerson.sendKeys("test");

        applicationDescription = driver.findElement(By.id("textArea_div_13"));
        applicationDescription.sendKeys("test");

        Select impactChannel = new Select(driver.findElement(By.cssSelector("#combo_div_8")));
        System.out.println(impactChannel.getOptions());
        impactChannel.selectByVisibleText("Osobiście");

        isAppointment = driver.findElement(By.cssSelector("#div_11 > div > div.controls > label > input[type=\"checkbox\"]"));
        if (isAppointment.isSelected()) {
            isAppointment.click();
        }

//        Umowa INO10239288
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#div_62_2_1_1_1_1_1_1_1 > div:nth-child(1)"))); // może być niepotrzebne ze względu na wcześniej wypełnianą sekcję
        System.out.println(driver.findElements(By.className("dataTableSelectControl")).size());

        String rbId = lwe.get(3).getAttribute("id");
        System.out.println(rbId);
        String rbId2 = rbId.replace("select_", "selection"); // zwraca id z innym początkiem stąd zamiana
        System.out.println(rbId2);
        Thread.sleep(500);
        driver.findElement(By.id(rbId2)).click();

//        HBO MAX HD
        Thread.sleep(2000);
        String hboFieldCheck = lwe.get(5).getAttribute("id"); // TODO sprawdzić czemu czasami jest 8 a nie 34 (size)
        System.out.println(hboFieldCheck);
        String hboFieldCheck2 = hboFieldCheck.replace("select_", "selection");
        System.out.println(hboFieldCheck2);
        Thread.sleep(100);
        driver.findElement(By.id(hboFieldCheck2)).click();

//        Sekcja decyzja (Utwórz sprawę)
        Select decision = new Select(driver.findElement(By.cssSelector("#combo_div_59")));
        System.out.println(decision.getOptions());
        decision.selectByVisibleText("Utwórz sprawę");

//        Komentarz
        WebElement insertComment;
        insertComment = driver.findElement(By.cssSelector("#textArea_div_60"));
        insertComment.sendKeys("Testowy 01");

//        Dalej
        WebElement sendApplication;
        sendApplication = driver.findElement(By.cssSelector("#div_62_3_1_1_1_1_3_1_1_1 > div:nth-child(1) > button:nth-child(1)"));
        sendApplication.click();

//        Alert
        driver.findElement(By.cssSelector("#div_61 > div:nth-child(2) > div:nth-child(3) > a:nth-child(2)")).click(); // trochę trzeba poczekać

        System.out.println("poszło");
//        #com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1) do 4 etapu
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
