import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.fail;

public class KORUsterkaFourthStep {

    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {

        System.setProperty("webdriver.firefox.marionette", "C:\\geckodriver.exe");
        driver = new FirefoxDriver();
        baseUrl = "https://bpms-tst-app1.bpms.inea.ad:9443/ProcessPortal/login.jsp";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testKORTworzenieUsterki() throws Exception {

        driver.get(baseUrl);
        WebDriverWait wait = new WebDriverWait(driver, 10);

//        Logowanie
        WebElement userName;
        WebElement password;
        WebElement logIn;
        WebElement searchBar;
        WebElement clickCase;

        Thread.sleep(200);
        userName = driver.findElement(By.id("username"));

        userName.sendKeys("");
        password = driver.findElement(By.id("password"));
        password.sendKeys("");
        logIn = driver.findElement(By.id("log_in"));
        logIn.click();

//        Wyszukiwanie etapu
        searchBar = driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_task_list_SearchBar_0_input"));
        wait.until(ExpectedConditions.visibilityOf(searchBar)).sendKeys("Diagnostyka");
        searchBar.sendKeys(Keys.ENTER);
        searchBar.clear();

//        Klikanie etapu
        Thread.sleep(2000);
//        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#dueToday > div.dijitTitlePaneTitle.dijitOpen > div > span.dijitTitlePaneTextNode")));
        WebElement cc = driver.findElements(By.className("bpm-social-task-row-data-subject")).get(0);
        wait.until(ExpectedConditions.visibilityOf(cc));
        System.out.println(cc.getText());
        cc.click();

        System.out.println("Działa");

//        iframe
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)")));
        driver.switchTo().frame(driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)"))).manage().window().maximize();
//        driver.manage().window().maximize();

//        Ankieta/wybierz zestaw
        WebElement choosePackage;
        WebElement packageTextBox;
        WebElement nextButton1;
        WebElement nextButton2;
        WebElement nextButton3;
        WebElement nextButton4;
        WebElement nextButton5;
        WebElement nextButton6;
        WebElement radioButton1;
        WebElement radioButton2;
        WebElement radioButton3;
        WebElement radioButton4;
        WebElement radioButton5;
        WebElement radioButton6;
        WebElement commectField;
        WebElement nextButtonn;

        // trzeba będzie tu poczekać
        choosePackage = driver.findElement(By.cssSelector("#div_62_1_1 > div > div.defectQuestionnairePackage > div > div > div > div > button"));
        choosePackage.click();

        packageTextBox = driver.findElement(By.cssSelector(".show-tick > div:nth-child(2) > div:nth-child(1) > input:nth-child(1)"));
        packageTextBox.sendKeys("hbo");
        packageTextBox.sendKeys(Keys.ENTER);

        radioButton1 = driver.findElements(By.name("243837_answer")).get(1);
        radioButton1.click();

        nextButton1 = driver.findElement(By.cssSelector("button.btn:nth-child(5)"));
        nextButton1.click();

        radioButton2 = driver.findElements(By.name("243839_answer")).get(1);
        radioButton2.click();

        nextButton2 = driver.findElement(By.cssSelector("button.btn:nth-child(6)"));
        nextButton2.click();

        radioButton3 = driver.findElements(By.name("243840_answer")).get(1);
        radioButton3.click();

        nextButton3 = driver.findElement(By.cssSelector("div.defectQuestionnaireQuestion:nth-child(5) > button:nth-child(6)"));
        nextButton3.click();

        radioButton4 = driver.findElements(By.name("243843_answer")).get(1);
        radioButton4.click();

        nextButton4 = driver.findElement(By.cssSelector("#div_62_1_1 > div > div.defectQuestionnaireQuestions > div:nth-child(7) > button:nth-child(6)"));
        nextButton4.click();

        radioButton5 = driver.findElements(By.name("243845_answer")).get(1);
        radioButton5.click();

        nextButton5 = driver.findElement(By.cssSelector("#div_62_1_1 > div > div.defectQuestionnaireQuestions > div:nth-child(9) > button:nth-child(6)"));
        nextButton5.click();

        radioButton6 = driver.findElement(By.name("243842_answer"));
        radioButton6.click();

        commectField = driver.findElement(By.cssSelector("textarea.form-control"));
        commectField.sendKeys("Test selenium");

        nextButton6 = driver.findElement(By.cssSelector("#div_62_1_1 > div > div.defectQuestionnaireQuestions > div:nth-child(11) > button:nth-child(6)"));
        nextButton6.click();

//        TODO zmienić name na linkText z "Dalej" + oodać sekcję Decyzja
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
