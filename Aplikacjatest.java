

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Aplikacjatest {
    WebDriver driver;
    String baseUrl;
    boolean acceptNextAlert = true;
    StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {

        System.setProperty("webdriver.firefox.marionette", "C:\\geckodriver.exe");
        driver = new FirefoxDriver();
        baseUrl = "https://bpms-tst-app1.bpms.inea.ad:9443/ProcessPortal/login.jsp";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    private static final String USER_NAME = "";
    private static final String PASSWORD = "";

    @Test
    public void testKORTworzenieUsterki() throws Exception {

//    Base URL
        driver.get(baseUrl);


//    Logowanie
        WebDriverWait wait = new WebDriverWait(driver, 25);

        waitForElementById(driver, wait, "log_in");
        sendKeysWithId(driver, "username", USER_NAME);
        sendKeysWithId(driver, "password", PASSWORD);
        clickWithId(driver, "log_in");

        System.out.println("Zalogowano");
//    Nowa instancja Procesu Obsługi sprawy na DEV
//    driver.findElement(By.linkText("Proces obsługi sprawy (Main (Rozwój FSM))")).click();
//    Nowa instancja TST
//    driver.findElement(By.linkText("Proces obsługi sprawy")).click();
//    Gotowa instancja
        driver.get("https://bpms-tst-app1.bpms.inea.ad:9443/ProcessPortal/dashboards/TWP/BPM_WORK?tw.local.view=taskcompletion&tw.local.taskid=374943");

//    Zmiana frame
        String iframe = "#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)";

        waitForElementByCssSelector(driver, wait, iframe);
        switchToNewIframe(driver, iframe);

        System.out.println("Zmieniono iframe");

//    Uzupełnienie danych klienta
        String cssSelector = ".input-block-level>input"; // w tym wypadku cssSelector jest odnośnikiem do klassy -> all input w klasie

        String firstName = getIdWithStaticPlaceholderByCssSelector(driver, cssSelector, "Imię");
        waitForElementById(driver, wait, firstName);
        sendKeysWithId(driver, firstName, "");
        String lastName = getIdWithStaticPlaceholderByCssSelector(driver, cssSelector, "Nazwisko");
        waitForElementById(driver, wait, lastName);
        sendKeysWithId(driver, lastName, "");

        System.out.println("Dodano Jacka");

//    Wyszukanie klienta
        String buttonCss = ".buttonDiv>button";
        clickButtonWithCssSelector(driver, buttonCss, "Szukaj");

        System.out.println("Wyszukało Jacka");

//        Poniższe działa
//        clickRadioButtonWithIDKNumber(driver, ".center>div", ".dataTableSelectControl>input", "230128");
        clickRadioButtonWithIDKNumber(driver, ".center>div", ".dataTableSelectControl>input", "87215174");

//    Przejście dalej
        clickButtonWithCssSelector(driver, buttonCss, "Dalej");

        System.out.println("Koniec Etapu 1");

        switchToDefaultIframe();


//    JavascriptExecutor jse = (JavascriptExecutor)driver;
//    jse.executeScript("argument[0].click", nextStep); <--- stała

//    Koniec - Etap: Wyszukiwanie i potwierdzenie abonenta
    }
    private static void clickRadioButtonWithIDKNumber(WebDriver driver, String csspath, String radioButPath, String idkNumber ) {
        List<WebElement> allDataInRows = driver.findElements(By.cssSelector(csspath)); //".center>div" for 1st Step
        List<WebElement> radioButtons = driver.findElements(By.cssSelector(radioButPath)); //"".dataTableSelectControl>input""

        for (int i = 0; i < allDataInRows.size(); i++) {
            System.out.println(allDataInRows.get(i).getText() + " index: " + i); // to czego szukamy IDK

            if (allDataInRows.get(i).getText().equals(idkNumber)) {
                int radioButtIndex = i / 7;
                if (i == 0) {
                    radioButtons.get(0).click();
                    break;
                } else {
                    radioButtons.get(radioButtIndex).click();
                    break;
                }
            }
        }
    }

    private static void clickHardCodeRadioButtonForJacek(WebDriver driver) {
        WebElement radioButtonClick;
        String cssId;
        WebDriverWait wait = new WebDriverWait(driver, 25);

        radioButtonClick = driver.findElements(By.className("dataTableSelectControl")).get(2);
        cssId = "#div_4_1_1_1_1_1_8_1_r16 > div > button";
        waitForElementByCssSelector(driver, wait, cssId);
        radioButtonClick.click();
    }

    private static void clickButtonWithCssSelector(WebDriver driver, String cssPath, String textOnSite) {
        List<WebElement> list = driver.findElements(By.cssSelector(cssPath));
//        cssPath = możliwa jest konfiguracja button[class='btn btn-default']/tagName[class='className']
//        for (int i = 0; i < list.size(); i++) {
//            if (list.get(i).getText().equals(textOnSite)) {
//                list.get(i).click();
//                break;
//            }
//        }
        for (WebElement asd: list) {
            if (asd.getText().equals(textOnSite)) {
                asd.click();
                break;
            }
        }
    }

    private static void switchToDefaultIframe() {
        WebDriver driver = new FirefoxDriver();
        driver.switchTo().defaultContent();
    }

    private static void switchToNewIframe(WebDriver driver, String iframe) {
        WebElement we = driver.findElement(By.cssSelector(iframe));
        driver.switchTo().frame(we);
    }

    private static void waitForElementByCssSelector(WebDriver driver, WebDriverWait wait, String cssSelector) {
        WebElement cssVisibility = driver.findElement(By.cssSelector(cssSelector));
        wait.until(ExpectedConditions.visibilityOf(cssVisibility));
    }

    private static void waitForElementByLinkText(WebDriver driver, WebDriverWait wait, String linkText) {
        WebElement textVisibility = driver.findElement(By.linkText(linkText));
        wait.until(ExpectedConditions.visibilityOf(textVisibility));
    }

    private static void waitForElementById(WebDriver driver, WebDriverWait wait, String id) {
        WebElement idVisibility = driver.findElement(By.id(id));
        wait.until(ExpectedConditions.visibilityOf(idVisibility));
    }

    private static String getIdWithStaticPlaceholderByCssSelector(WebDriver driver, String cssPath, String staticPlaceHolder) {
        List<WebElement> list = driver.findElements(By.cssSelector(cssPath));
        String res = null;

        for (WebElement aList : list) {
            String placeholder = driver.findElement(By.xpath("//input[@placeholder='" + staticPlaceHolder + "']")).getAttribute("placeholder");
            if (aList.getAttribute("placeholder").equals(placeholder)) {
                res = aList.getAttribute("id");
                break;
            }
        }
        return res;
    }

    private static void sendKeysWithId(WebDriver driver, String id, CharSequence text) {
        WebElement we = driver.findElement(By.id(id));
        we.sendKeys(text);
    }

    private static void clickWithId(WebDriver driver, String id) {
        WebElement we = driver.findElement(By.id(id));
        we.click();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
