

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class KORUsterkaApp {
    WebDriver driver;
    String baseUrl;
    boolean acceptNextAlert = true;
    StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {

        System.setProperty("webdriver.firefox.marionette", "C:\\geckodriver.exe");
        driver = new FirefoxDriver();
        baseUrl = "https://bpms-tst-app1.bpms.inea.ad:9443/ProcessPortal/login.jsp";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    private static final String USER_NAME = "";
    private static final String PASSWORD = "";

    @Test
    public void testKORTworzenieUsterki() throws Exception {

//    Base URL
        driver.get(baseUrl);


//    Logowanie
        WebDriverWait wait = new WebDriverWait(driver, 25);

        waitForElementById(driver, wait, "log_in");
        sendKeysWithId(driver, "username", USER_NAME);
        sendKeysWithId(driver, "password", PASSWORD);
        clickWithId(driver, "log_in");

        System.out.println("Zalogowano");
//    Nowa instancja Procesu Obsługi sprawy na DEV
//    driver.findElement(By.linkText("Proces obsługi sprawy (Main (Rozwój FSM))")).click();
//    Nowa instancja TST
    driver.findElement(By.linkText("Proces obsługi sprawy")).click();
//    Gotowa instancja
//        driver.get("https://bpms-tst-app1.bpms.inea.ad:9443/ProcessPortal/dashboards/TWP/BPM_WORK?tw.local.view=taskcompletion&tw.local.taskid=374943");

//    Zmiana frame
        String iframe = "#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)";

        waitForElementByCssSelector(driver, wait, iframe);
        switchToNewIframe(driver, iframe);

        System.out.println("Zmieniono iframe");

//    Uzupełnienie danych klienta
        String cssSelector = ".input-block-level>input"; // w tym wypadku cssSelector jest odnośnikiem do klassy -> all input w klasie

        String firstName = getIdWithStaticPlaceholderByCssSelector(driver, cssSelector, "Imię");
        waitForElementById(driver, wait, firstName);
        sendKeysWithId(driver, firstName, "");
        String lastName = getIdWithStaticPlaceholderByCssSelector(driver, cssSelector, "Nazwisko");
        waitForElementById(driver, wait, lastName);
        sendKeysWithId(driver, lastName, "");

        System.out.println("Dodano Jacka");

//    Wyszukanie klienta
        String buttonCss = ".buttonDiv>button";
        clickButtonWithCssSelector(driver, buttonCss, "Szukaj");

        System.out.println("Wyszukało Jacka");

//        Poniższe działa
//        clickRadioButtonWithIDKNumber(driver, ".center>div", ".dataTableSelectControl>input", "230128");
        clickRadioButtonWithIDKNumber(driver, ".center>div", ".dataTableSelectControl>input", "87215174");

//    Przejście dalej
        clickButtonWithCssSelector(driver, buttonCss, "Dalej");

        System.out.println("Koniec Etapu 1");

        switchToDefaultIframe();

//        *******************************************ETAP II****************************************************


        Thread.sleep(10500); // <--- TODO zrobić tak, żeby czekało na załadowanie iframe w całości razem z umowami.
        WebElement frame = driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)"));
        driver.switchTo().frame(frame);

//        Wypełnienie formularza
        Select sel = new Select(driver.findElement(By.cssSelector("#combo_div_41")));
        System.out.println(sel.getOptions());
        sel.selectByVisibleText("Usterka");
//        sel.selectByIndex(1); - też UST
//        driver.findElement(By.cssSelector("option[value=\"Usterka\"]")).click();

//        new Select(driver.findElement(By.id("combo_div_2"))).selectByVisibleText("Usterka");
//        driver.findElement(By.cssSelector("option[value=\"Usterka\"]")).click();
//        new Select(driver.findElement(By.cssSelector("#combo_div_41"))).selectByVisibleText("Usterka");
        System.out.println("działa");

//        Dalej
        driver.findElement(By.cssSelector("#div_42_3_1_1_1_1_3_1_1 > div > button")).click();

        System.out.println("Koniec drugiego etapu");

        driver.switchTo().defaultContent();

//    *********************************************************ETAP III****************************************************

//    iframe
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)")));
        driver.switchTo().frame(driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)")));
        driver.manage().window().maximize();

//    Formularz
        WebElement smsNotification;
        WebElement contactPerson;
        WebElement applicationDescription;
        WebElement isAppointment;

        List<WebElement> lwe = driver.findElements(By.className("dataTableSelectControl"));

//        Sekcja klasyfikacja sprawy
        smsNotification = driver.findElement(By.cssSelector("#div_7 > div > div.controls > label > input[type=\"checkbox\"]"));
        if (smsNotification.isSelected()) {
            smsNotification.click();
        }

        contactPerson = driver.findElement(By.cssSelector("#input_div_6_1_1_1_1_1_1_3"));
        contactPerson.sendKeys("test");

        applicationDescription = driver.findElement(By.id("textArea_div_13"));
        applicationDescription.sendKeys("test");

        Select impactChannel = new Select(driver.findElement(By.cssSelector("#combo_div_8")));
        System.out.println(impactChannel.getOptions());
        impactChannel.selectByVisibleText("Osobiście");

        isAppointment = driver.findElement(By.cssSelector("#div_11 > div > div.controls > label > input[type=\"checkbox\"]"));
        if (isAppointment.isSelected()) {
            isAppointment.click();
        }

//        Umowa INO10239288
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#div_62_2_1_1_1_1_1_1_1 > div:nth-child(1)"))); // może być niepotrzebne ze względu na wcześniej wypełnianą sekcję
        System.out.println(driver.findElements(By.className("dataTableSelectControl")).size());

        String rbId = lwe.get(3).getAttribute("id");
        System.out.println(rbId);
        String rbId2 = rbId.replace("select_", "selection"); // zwraca id z innym początkiem stąd zamiana
        System.out.println(rbId2);
        Thread.sleep(500);
        driver.findElement(By.id(rbId2)).click();

//        HBO MAX HD
        Thread.sleep(2000);
        String hboFieldCheck = lwe.get(5).getAttribute("id"); // TODO sprawdzić czemu czasami jest 8 a nie 34 (size)
        System.out.println(hboFieldCheck);
        String hboFieldCheck2 = hboFieldCheck.replace("select_", "selection");
        System.out.println(hboFieldCheck2);
        Thread.sleep(100);
        driver.findElement(By.id(hboFieldCheck2)).click();

//        Sekcja decyzja (Utwórz sprawę)
        Select decision = new Select(driver.findElement(By.cssSelector("#combo_div_59")));
        System.out.println(decision.getOptions());
        decision.selectByVisibleText("Utwórz sprawę");

//        Komentarz
        WebElement insertComment;
        insertComment = driver.findElement(By.cssSelector("#textArea_div_60"));
        insertComment.sendKeys("Testowy 01");

//        Dalej
        WebElement sendApplication;
        sendApplication = driver.findElement(By.cssSelector("#div_62_3_1_1_1_1_3_1_1_1 > div:nth-child(1) > button:nth-child(1)"));
        sendApplication.click();

//        Alert
        driver.findElement(By.cssSelector("#div_61 > div:nth-child(2) > div:nth-child(3) > a:nth-child(2)")).click(); // trochę trzeba poczekać

        System.out.println("poszło");

        driver.switchTo().defaultContent();

//    *******************************************************************ETAP IV*************************************************

        //        iframe
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)")));
        driver.switchTo().frame(driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)"))).manage().window().maximize();
//        driver.manage().window().maximize();

//        Ankieta/wybierz zestaw
        WebElement choosePackage;
        WebElement packageTextBox;
        WebElement nextButton1;
        WebElement nextButton2;
        WebElement nextButton3;
        WebElement nextButton4;
        WebElement nextButton5;
        WebElement nextButton6;
        WebElement radioButton1;
        WebElement radioButton2;
        WebElement radioButton3;
        WebElement radioButton4;
        WebElement radioButton5;
        WebElement radioButton6;
        WebElement commectField;
        WebElement nextButtonn;

        // trzeba będzie tu poczekać
        choosePackage = driver.findElement(By.cssSelector("#div_62_1_1 > div > div.defectQuestionnairePackage > div > div > div > div > button"));
        choosePackage.click();

        packageTextBox = driver.findElement(By.cssSelector(".show-tick > div:nth-child(2) > div:nth-child(1) > input:nth-child(1)"));
        packageTextBox.sendKeys("hbo");
        packageTextBox.sendKeys(Keys.ENTER);

        radioButton1 = driver.findElements(By.name("243837_answer")).get(1);
        radioButton1.click();

        nextButton1 = driver.findElement(By.cssSelector("button.btn:nth-child(5)"));
        nextButton1.click();

        radioButton2 = driver.findElements(By.name("243839_answer")).get(1);
        radioButton2.click();

        nextButton2 = driver.findElement(By.cssSelector("button.btn:nth-child(6)"));
        nextButton2.click();

        radioButton3 = driver.findElements(By.name("243840_answer")).get(1);
        radioButton3.click();

        nextButton3 = driver.findElement(By.cssSelector("div.defectQuestionnaireQuestion:nth-child(5) > button:nth-child(6)"));
        nextButton3.click();

        radioButton4 = driver.findElements(By.name("243843_answer")).get(1);
        radioButton4.click();

        nextButton4 = driver.findElement(By.cssSelector("#div_62_1_1 > div > div.defectQuestionnaireQuestions > div:nth-child(7) > button:nth-child(6)"));
        nextButton4.click();

        radioButton5 = driver.findElements(By.name("243845_answer")).get(1);
        radioButton5.click();

        nextButton5 = driver.findElement(By.cssSelector("#div_62_1_1 > div > div.defectQuestionnaireQuestions > div:nth-child(9) > button:nth-child(6)"));
        nextButton5.click();

        radioButton6 = driver.findElement(By.name("243842_answer"));
        radioButton6.click();

        commectField = driver.findElement(By.cssSelector("textarea.form-control"));
        commectField.sendKeys("Test selenium");

        nextButton6 = driver.findElement(By.cssSelector("#div_62_1_1 > div > div.defectQuestionnaireQuestions > div:nth-child(11) > button:nth-child(6)"));
        nextButton6.click();

        Select dec = new Select(driver.findElement(By.cssSelector("#combo_div_64")));
        System.out.println(decision.getOptions());
        decision.selectByVisibleText("Usterka nierozwiązana");

        WebElement kom = driver.findElement(By.cssSelector("#textArea_div_65"));
        kom.sendKeys("Test kom");

        WebElement endBut = driver.findElement(By.cssSelector("#div_78_3_1_1_1_1_3_1_1_1 > div > button"));
        endBut.click();




//    JavascriptExecutor jse = (JavascriptExecutor)driver;
//    jse.executeScript("argument[0].click", nextStep); <--- stała

//    Koniec - Etap: Wyszukiwanie i potwierdzenie abonenta


//    JavascriptExecutor jse = (JavascriptExecutor)driver;
//    jse.executeScript("argument[0].click", nextStep); <--- stała

//    Koniec - Etap: Wyszukiwanie i potwierdzenie abonenta
    }
    private static void clickRadioButtonWithIDKNumber(WebDriver driver, String csspath, String radioButPath, String idkNumber ) {
        List<WebElement> allDataInRows = driver.findElements(By.cssSelector(csspath)); //".center>div" for 1st Step
        List<WebElement> radioButtons = driver.findElements(By.cssSelector(radioButPath)); //"".dataTableSelectControl>input""

        for (int i = 0; i < allDataInRows.size(); i++) {
            System.out.println(allDataInRows.get(i).getText() + " index: " + i); // to czego szukamy IDK

            if (allDataInRows.get(i).getText().equals(idkNumber)) {
                int radioButtIndex = i / 7;
                if (i == 0) {
                    radioButtons.get(0).click();
                    break;
                } else {
                    radioButtons.get(radioButtIndex).click();
                    break;
                }
            }
        }
    }

    private static void clickHardCodeRadioButtonForJacek(WebDriver driver) {
        WebElement radioButtonClick;
        String cssId;
        WebDriverWait wait = new WebDriverWait(driver, 25);

        radioButtonClick = driver.findElements(By.className("dataTableSelectControl")).get(2);
        cssId = "#div_4_1_1_1_1_1_8_1_r16 > div > button";
        waitForElementByCssSelector(driver, wait, cssId);
        radioButtonClick.click();
    }

    private static void clickButtonWithCssSelector(WebDriver driver, String cssPath, String textOnSite) {
        List<WebElement> list = driver.findElements(By.cssSelector(cssPath));
//        cssPath = możliwa jest konfiguracja button[class='btn btn-default']/tagName[class='className']
//        for (int i = 0; i < list.size(); i++) {
//            if (list.get(i).getText().equals(textOnSite)) {
//                list.get(i).click();
//                break;
//            }
//        }
        for (WebElement asd: list) {
            if (asd.getText().equals(textOnSite)) {
                asd.click();
                break;
            }
        }
    }

    private static void switchToDefaultIframe() {
        WebDriver driver = new FirefoxDriver();
        driver.switchTo().defaultContent();
    }

    private static void switchToNewIframe(WebDriver driver, String iframe) {
        WebElement we = driver.findElement(By.cssSelector(iframe));
        driver.switchTo().frame(we);
    }

    private static void waitForElementByCssSelector(WebDriver driver, WebDriverWait wait, String cssSelector) {
        WebElement cssVisibility = driver.findElement(By.cssSelector(cssSelector));
        wait.until(ExpectedConditions.visibilityOf(cssVisibility));
    }

    private static void waitForElementByLinkText(WebDriver driver, WebDriverWait wait, String linkText) {
        WebElement textVisibility = driver.findElement(By.linkText(linkText));
        wait.until(ExpectedConditions.visibilityOf(textVisibility));
    }

    private static void waitForElementById(WebDriver driver, WebDriverWait wait, String id) {
        WebElement idVisibility = driver.findElement(By.id(id));
        wait.until(ExpectedConditions.visibilityOf(idVisibility));
    }

    private static String getIdWithStaticPlaceholderByCssSelector(WebDriver driver, String cssPath, String staticPlaceHolder) {
        List<WebElement> list = driver.findElements(By.cssSelector(cssPath));
        String res = null;

        for (WebElement aList : list) {
            String placeholder = driver.findElement(By.xpath("//input[@placeholder='" + staticPlaceHolder + "']")).getAttribute("placeholder");
            if (aList.getAttribute("placeholder").equals(placeholder)) {
                res = aList.getAttribute("id");
                break;
            }
        }
        return res;
    }

    private static void sendKeysWithId(WebDriver driver, String id, CharSequence text) {
        WebElement we = driver.findElement(By.id(id));
        we.sendKeys(text);
    }

    private static void clickWithId(WebDriver driver, String id) {
        WebElement we = driver.findElement(By.id(id));
        we.click();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
