
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.List;
import java.util.NoSuchElementException;

public class MethodsBackUp {

    final static String iframeCondition = "javascript";

    static WebDriver driver;
    static WebDriverWait wait;
    static WebDriverWait longWait;
    static JavascriptExecutor js;
    String chromeURL;
    String tmpa;

    public static void logToPortal(String login, String pass) {
        WebElement userid;
        WebElement password;

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        userid = driver.findElement(By.id("username"));
        userid.sendKeys(login);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
        password = driver.findElement(By.id("password"));
        password.sendKeys(pass);

//        W INEA trzeba tak
        try {
            driver.findElement(By.cssSelector("#signInForm > a")).click();
        } catch (Exception e) {
            System.err.println("Logowanie nie przeszło metodą logToPortal.");
        }

    }

    public static void browserChromeDriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
        MethodsBackUp.driver = new ChromeDriver();
        wait = new WebDriverWait(MethodsBackUp.driver, 35);
        longWait = new WebDriverWait(MethodsBackUp.driver, 200);
    }

    public MethodsBackUp(String chromeURL) {
        super();
        System.setProperty("webdriver.chrome.driver", chromeURL);
        MethodsBackUp.driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 35);
        longWait = new WebDriverWait(driver, 200);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        MethodsBackUp.driver = driver;
    }

    public String getChromeDriver() {
        return chromeURL;
    }

    public void setChromeDriver(String chromeURL) {
        this.chromeURL = chromeURL;
    }

    public static void startTest(String url) {
        driver.manage().window().maximize();
        driver.get(url);

    }


    public static void newTask(String taskName) {
        List<WebElement> processesList = driver.findElements(By.className("menu-link-body"));
        for (int i = 0; i < processesList.size(); i++) {
            System.out.println(processesList.get(i).getText());

            if (processesList.get(i).getText().contains(taskName)) {
                System.out.println(i);
                processesList.get(i).click();
                break;
            }
        }
    }

    public static void newTask(String taskName, String snapshotName) {
        List<WebElement> processesList = driver.findElements(By.className("menu-link-body"));
        for (int i = 0; i < processesList.size(); i++) {
            System.out.println(processesList.get(i).getText());

            if (processesList.get(i).getText().contains(taskName)) {
                System.out.println(i);
                List<WebElement> iconList = driver.findElements(By.className("menu-link-icon"));
                iconList.get(i).click();
                break;
            }
        }
        WebElement txt = driver.findElement(By.partialLinkText(snapshotName));
        if (txt != null) {
            txt.click();
        }
    }

    public static void newTask(String taskName, String snapshotName, String trackName) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("menu-link-body")));
        List<WebElement> processesList = driver.findElements(By.className("menu-link-body"));
        for (int i = 0; i < processesList.size(); i++) {
            System.out.println(processesList.get(i).getText());

            if (processesList.get(i).getText().contains(taskName) && processesList.get(i).getText().contains(trackName)) {
                System.out.println(i);
                List<WebElement> iconList = driver.findElements(By.className("menu-link-icon"));
                iconList.get(i).click();
                break;
            } else {
                continue;
            }
        }
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText(snapshotName)));
        WebElement txt = driver.findElement(By.partialLinkText(snapshotName));
        if (txt != null) {
            txt.click();
        }
    }

    public static void newTaskWithTrack(String taskName, String trackName) {
        List<WebElement> processesList = driver.findElements(By.className("menu-link-body"));
        for (int i = 0; i < processesList.size(); i++) {
            System.out.println(processesList.get(i).getText());

            if (processesList.get(i).getText().contains(taskName) && processesList.get(i).getText().contains(trackName)) {
                System.out.println(i);
                processesList.get(i).click();
                break;
            } else {
                continue;
            }
        }
    }

    public static void setIframe(int sleep) throws InterruptedException {
        List <WebElement> iframes = driver.findElements(By.tagName("iframe"));
        int i = 0;
        System.out.println(iframes.size());
        for (WebElement webElement : iframes) {
            String iframe = iframes.get(i).getAttribute("ng-attr-title");
            if (iframe != null) {
                if (iframe.equals("{{coachTitle}}")) {
                    WebElement iframeToSwitch = iframes.get(i);

                    driver.switchTo().frame(iframeToSwitch);
                    Thread.sleep(sleep);
                    System.out.println("ustawil iframe 1");

                    List <WebElement> iframesIn = driver.findElements(By.tagName("iframe"));
//     System.out.println("zagniezdzony: " + iframesIn.get(0).getAttribute("src"));
                    System.out.println("Iframe zagniezdzone: " + iframesIn.size());
                    if (iframesIn.size() >= 1 && !iframesIn.get(0).getAttribute("src").contains(iframeCondition)) {
                        WebElement internalIFrame;
                        internalIFrame = iframesIn.get(0);
                        driver.switchTo().frame(internalIFrame);
                        System.out.println("ustawil iframe 2");
                    }
                    break;
                }
            }
            i++;
        }
    }

    public static void selectValueFromDropDown(String labelText, String valueText) {
        List<WebElement> list = driver.findElements(By.className("form-group"));
        List<WebElement> lista2 = driver.findElements(By.xpath("//div[contains(@class, 'control')]//label[contains(@class, 'control')]"));

        boolean tmp = false;

        for (int i = 0; i < list.size(); i++) {

            if (list.size() > 0 && list.get(i).getText() != null && !list.get(i).getText().equals("") && list.get(i).getText().contains(labelText)) {
                    List<WebElement> selectList = driver.findElements(By.xpath("//select[starts-with(@class, 'form-control col-xs-')]"));

                for (int j = 0; j < selectList.size(); j++) {
                    String id = selectList.get(j).getAttribute("id");

                    if (selectList.get(j).getText().contains(valueText) && list.get(i).findElement(By.tagName("label")).getAttribute("for").equals(id)) {
                        Select sel = new Select(driver.findElement(By.id(id)));
                        sel.selectByVisibleText(valueText);
                        tmp = true;
                        break;
                    }
                }
                break;
            }
        }
        if (!tmp) {
            for (int i = 0; i < lista2.size(); i++) {
                System.out.println(lista2.get(i).findElement(By.tagName("label")).getText());

                if (lista2.get(i).getText().contains(labelText)) {
                    List<WebElement> selectList2 = driver.findElements(By.xpath("//*[contains(@class, 'control')]//select"));

                    for (int j = 0; j < selectList2.size(); j++) {
                        String id = selectList2.get(j).getAttribute("id");

                        if (selectList2.get(j).getText().contains(valueText) && lista2.get(i).getAttribute("for").equals(id)) {
                            Select sel = new Select(driver.findElement(By.id(id)));
                            sel.selectByVisibleText(valueText);
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }

//    *******Metoda działa, ale nie sprawdzana na innych procesach oprócz INEA******
//    private static void buttonClickByText(String textOnButton) {
//        List<WebElement> list = driver.findElements(By.xpath("//*[contains(@class, 'btn')]"));
////        zawęża poszukiwania, ale nie jest niezbędny.
////        List<WebElement> list2 = driver.findElements(By.xpath("//div[contains(@class, documentList)]//button[contains(@class, 'btn')]"));
//
//        if (list.size() == 1) {
//            try {
//                list.get(0).click();
//            } catch (WebDriverException e) {
//                System.err.println("Przycisku nie da się obsłużyć click'iem");
//                list.get(0).sendKeys(Keys.ENTER);
//            }
//        } else {
//            for (int i = 0; i < list.size(); i++) {
//                System.out.println(list.get(i).getText());
//                if (list.get(i).getText().contains(textOnButton)) {
//                    try {
//                        list.get(i).click();
//                    } catch (WebDriverException e) {
//                        System.err.println("Przycisku nie da się obsłużyć click'iem");
//                        list.get(i).sendKeys(Keys.ENTER);
//                    }
//                    break;
//                }
//            }
//
//        }
//    }

    private static void buttonClickByText(String textOnButton) throws InterruptedException {
        List<WebElement> list = driver.findElements(By.xpath("//*[contains(@class, 'btn')]"));
//        zawęża poszukiwania, ale nie jest niezbędny.
//        List<WebElement> list2 = driver.findElements(By.xpath("//div[contains(@class, documentList)]//button[contains(@class, 'btn')]"));

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getText() != null && !list.get(i).getText().equals("")) {
                if (list.get(i).getText().equals(textOnButton) || list.get(i).getText().equals(" "+textOnButton)) {
                    try {
                        list.get(i).click();
                        Thread.sleep(2000);
                    } catch (WebDriverException e) {
                        System.err.println("Przycisku nie da się obsłużyć kliknięciem. Wciskam ENTER");
                        list.get(i).sendKeys(Keys.ENTER);
                        Thread.sleep(2000);
                    }
                    break;
                }
            }
        }
    }

    private static void modalButtonClick(String textOnButton) throws InterruptedException {
//        TODO Dodać obsługę wait na buttonie bo mija sec zanim się pojawi
        Thread.sleep(1000);
        List<WebElement> modal = driver.findElements(By.xpath("//*[contains(@class, 'modal')]//*[contains(@class, 'btn')]"));
        for (int i = 0; i < modal.size(); i++) {
            if (modal.get(i).getText().contains(textOnButton)) {
//                pozostała obsługa opcji, kiedy element jest niewidoczny || nie da się jeszcze w niego kliknąć
                modal.get(i).click();
            } else if (modal.get(i).getTagName().contains("input") && modal.get(i).getAttribute("value").contains(textOnButton)) {
                modal.get(i).sendKeys(Keys.ENTER);
            } else {
                continue;
            }
        }
    }

    private static void documentAdd(String docPath, String fileNameDotFormat) throws InterruptedException {
        // do kontrolki bez modala
        List<WebElement> upload = driver.findElements(By.xpath("//div[contains(@class, 'upload-')]//input"));
        // do kontrolki w modalu
        List<WebElement> uploadModal = driver.findElements(By.xpath("//div[contains(@class, 'modal')]//*[contains(@type, 'file')]"));

        if (upload.size() > 0) {
            for (int i = 0; i < upload.size(); i++) {

                if (upload.get(i).isEnabled()) {
                    File file = new File(docPath);
                    String getFilePath = file.getAbsolutePath();
                    upload.get(i).sendKeys(getFilePath + "\\" + fileNameDotFormat);
                    break;
                }
            }
        } else {
            // do przycisku Dodawania dokumentu w modalu
            System.err.println("Dokument posiada modal. Nie ma parametru 'modalButtonText'");
            Thread.sleep(1000);

            try {
                for (int i = 0; i < uploadModal.size(); i++) {

                    if (uploadModal.get(i).getAttribute("name").contains("file")) {
                        File file = new File(docPath);
                        String getFilePath = file.getAbsolutePath();
                        uploadModal.get(i).sendKeys(getFilePath + "\\" + fileNameDotFormat);
                        break;
                    }
                }
            } catch (Exception e) {
                System.err.println("Dokument nie dodane przez brak parametru 'modaButtonText'");;
            }
        }
    }

    private static void documentAdd(String docPath, String fileNameDotFormat, String modalButtonText) throws InterruptedException {
//        TODO Modal nie jest z początku w ogóle włączony. Trzeba to zweryfikować biznesowo i sprawić, żeby button działał
//        TODO możliwe, że bez tego nie przejdzie.
        // do kontrolki bez modala
        List<WebElement> upload = driver.findElements(By.xpath("//div[contains(@class, 'upload-')]//input"));
        // do kontrolki w modalu
        List<WebElement> uploadModal = driver.findElements(By.xpath("//div[contains(@class, 'modal')]//*[contains(@type, 'file')]"));

        if (upload.size() > 0) {
            for (int i = 0; i < upload.size(); i++) {

                if (upload.get(i).isEnabled()) {
                    File file = new File(docPath);
                    String getFilePath = file.getAbsolutePath();
                    upload.get(i).sendKeys(getFilePath + "\\" + fileNameDotFormat);
                    break;
                }
            }
        } else {
            // do przycisku Dodawania dokumentu w modalu
            Thread.sleep(1000);
            for (int i = 0; i < uploadModal.size(); i++) {

                if (uploadModal.get(i).getAttribute("name").contains("file")) {
                    File file = new File(docPath);
                    String getFilePath = file.getAbsolutePath();
                    uploadModal.get(i).sendKeys(getFilePath + "\\" + fileNameDotFormat);
                    break;
                }
            }
            modalButtonClick(modalButtonText);
        }
    }

    public static void datePick(String labelText, String year, String month, String day) {
        List<WebElement> list = driver.findElements(By.className("form-group"));
        List<WebElement> list2 = driver.findElements(By.xpath("//div[contains(@class, 'control')]"));

        boolean tmp = false;

        for (WebElement aList : list) {
            if (list.size() > 0 && !aList.getText().equals("") && aList.getText() != null) {

                if (aList.getText().contains(labelText)) {
                    List<WebElement> dateList = driver.findElements(By.xpath("//input[starts-with(@class, 'someDate')]"));

                    for (WebElement aDateList : dateList) {
                        String id = aDateList.getAttribute("id");

                        if (aList.findElement(By.tagName("label")).getAttribute("for").equals(id)) {
                            aDateList.sendKeys(year + "-" + month + "-" + day);
                            tmp = true;
                            break;
                        }
                    }
                    break;
                }
            }
        }

        if (!tmp) {
            for (WebElement aLst2 : list2) {
                if (list2.size() > 0 && !aLst2.getText().equals("") && aLst2.getText() != null) {

                    if (aLst2.getText().contains(labelText)) {
                        List<WebElement> dateList2 = driver.findElements(By.xpath("//input[contains(@class, 'someDate')]"));

                        for (WebElement aDateList2 : dateList2) {
                            String id = aDateList2.getAttribute("id");

                            if (aLst2.findElement(By.tagName("label")).getAttribute("for").equals(id)) {
                                aLst2.click();
                                aDateList2.sendKeys(year + month + day);
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }
    }

    private static void radioButtonClickByText(String radioButtonLabelText) {
        List<WebElement> r = driver.findElements(By.xpath("//label[starts-with(@class, 'radio')]"));

        for (int i = 0; i < r.size(); i++) {
            System.out.println(r.get(i).getText());
            if (r.get(i).getText().equals(radioButtonLabelText) && r.get(i).getText().equals(" "+radioButtonLabelText) && r.get(i).getText().equals("  "+radioButtonLabelText)) {
                r.get(i).click();
                break;
            }
        }
    }

    public static void selectValueFromDropDownTest(String labelText, String valueText) {
        List<WebElement> list = driver.findElements(By.className("form-group"));
        List<WebElement> lista2 = driver.findElements(By.xpath("//*[contains(@class, 'control')]//select"));

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getText());

            if (list.get(i).getText().contains(labelText)) {
                List<WebElement> lista24 = driver.findElements(By.xpath("//select[starts-with(@class, 'form-control col-xs-')]"));

                for (int j = 0; j < lista24.size(); j++) {
//                zwraca listę value/options oraz id z rozwijanych select
                    System.out.println(lista24.get(j).getText());
                    System.out.println(lista24.get(j).getAttribute("id"));
                    String id = lista24.get(j).getAttribute("id");

//                    lista24.get(j).getText().contains(valueText) && list.get(i).getText().contains(labelText) && list.get(i).getText().contains(valueText)
                    if (lista24.get(j).getText().contains(valueText) && list.get(i).findElement(By.tagName("label")).getAttribute("for").equals(id)) {
                        Select sel = new Select(driver.findElement(By.id(id)));
                        System.out.println(lista24.get(j).getText());
                        sel.selectByVisibleText(valueText);
                        break;
                    } else {
                        continue;
                    }
                }
                break;
            } //if (list.get(i).getText().contains(labelText)) {
        }

        for (int i = 0; i < lista2.size(); i++) {
            System.out.println(lista2.get(i).getText());

            if (lista2.get(i).getText().contains(labelText)) {

                List<WebElement> ra = driver.findElements(By.xpath("//*[contains(@class, 'control')]//select"));

                for (int j = 0; j < ra.size(); j++) {
                    System.out.println(ra.get(i).getText());
                    System.out.println(ra.get(i).getAttribute("id"));
                    String id = ra.get(i).getAttribute("id");

                    if (ra.get(j).getText().contains(valueText) && lista2.get(i).findElement(By.tagName("label")).getAttribute("for").equals(id)) {
                        Select sel = new Select(driver.findElement(By.id(id)));
                        System.out.println(lista2.get(j).getText());
                        sel.selectByVisibleText(valueText);
                        break;
                    }
                }

            } else {//if (lista2.get(i).getText().contains(labelText)) {
                System.err.println("coś poszło nie tak");
                break;
            }
        }
    }

    /*
          Try catch koniecznie w else if dalej w metodzie trzeba dodać
      */
    public static void fillInput(String labelName, String value) throws InterruptedException {
        // wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("control-label")));
        Thread.sleep(3000);
        List<WebElement> listOfInputs = driver.findElements(By.className("control-label"));
        System.out.println(listOfInputs.size());

        for (int i = 0; i < listOfInputs.size(); i++) {
            List<WebElement> spans = listOfInputs.get(i).findElements(By.tagName("span"));
            if (spans.size() > 0) {
                // System.out.println("element: " + spans.get(0).getAttribute("textContent"));
                if (spans.get(0).getAttribute("textContent").equals(labelName)) {
                    WebElement parent = listOfInputs.get(i).findElement(By.xpath(".."));
                    parent = parent.findElement(By.xpath(".."));
                    if (checkFieldInput(parent)) {
                        try {
                            System.out.println("parent" + parent.getAttribute("class"));
                            List<WebElement> inputs = parent.findElements(By.tagName("input"));
                            System.out.println("lenght " + inputs.size());
                            System.out.println("spann " + spans.get(0).getAttribute("textContent"));
                            System.out.println("id " + inputs.get(0).getAttribute("id") + "classs " + inputs.get(0).getAttribute("class"));
                            System.out.println("**tag " + inputs.get(0).getAttribute("tagName"));
//                        wait.until(ExpectedConditions.visibilityOf(inputs.get(0)));
                            js.executeScript("arguments[0].click();", inputs.get(0));
                            inputs.get(0).clear();
                            inputs.get(0).sendKeys(value);
                            break;
                        } catch (ElementNotVisibleException e) {
                            continue;
                        } catch (InvalidElementStateException s) {
                            continue;
                        }

                    } else if (checkFieldTextArea(parent)) {
                        List<WebElement> textareas = parent.findElements(By.tagName("textarea"));
                        System.out.println("lenght " + textareas.size());
                        js.executeScript("arguments[0].click();", textareas.get(0));
                        textareas.get(0).clear();
                        textareas.get(0).sendKeys(value);
                        break;
                    }
                }
            } else {
                if (listOfInputs.get(i).getAttribute("textContent").equals(labelName)) {
                    js.executeScript("arguments[0].click();", listOfInputs.get(i));
                    driver.switchTo().activeElement().clear();
                    driver.switchTo().activeElement().sendKeys(value);
                    System.out.println("Znalazł pole: " + labelName + " po control-label");
                    break;
                }
            }
        }
    }

    public static boolean checkFieldTextArea(WebElement field) {
        try {
            field.findElement(By.tagName("textarea"));
            return true;

        } catch (NoSuchElementException e) {
            // System.out.println("Nie znalazĹ‚ elementu !!!");
            return false;
        }
    }

    public static boolean checkFieldInput(WebElement field) {
        try {

            field.findElement(By.tagName("input"));

            return true;

        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private static void clickDeleteButton(String sectionText, int row) {
        List<WebElement> h = driver.findElements(By.xpath("//div[contains(@role, 'heading')]"));

        for (int i = 0; i < h.size(); i++) {

            if (h.get(i).getText().contains(sectionText)) {
                String id1 = h.get(i).getAttribute("id");
                String id2 = id1.replace("collapseHeader_", "");

                if (!id1.contains("collapseHeader_")) {
                    System.err.println("Sekcja posiada inne ID niż collapseHeader. Zgłosić do DEV");
                    break;
                }

                List<WebElement> h2 = driver.findElements(By.xpath("//div[contains(@id, '" + id2 + "')]//i[contains(@class, 'trash')]"));

                for (int j = 0; j < h2.size(); j++) {
                    if (j == row-1) {
                        wait.until(ExpectedConditions.visibilityOf(h2.get(j)));
                        h2.get(j).click();
                        break;
                    }
                }
                break;
            }
        }
    }

    private static void deleteButtonClick(int row) {
        List<WebElement> list = driver.findElements(By.xpath("//*[contains(@class, 'btn')]//i[contains(@class, 'trash')]"));
        for (int i = 0; i < list.size(); i++) {

            if (i == row-1)
                list.get(i).click();
        }
    }

    public static void radioButtonClickInTabel(String labelText, String radioButtonText) throws InterruptedException {
        List<WebElement> ra = driver.findElements(By.xpath("//div[starts-with(@class, 'form-group')]//*[contains(@class, 'radio')]"));
        List<WebElement> ca2 = driver.findElements(By.xpath("//div[contains(@class, 'form')]//label[contains(@class, 'control')]"));

        for (int i = 0; i < ca2.size(); i++) {
            String labelName = ca2.get(i).getText();
            String idLabelNumber = ca2.get(i).getAttribute("id");
            String id2 = idLabelNumber.replace("label_", "");

            if (labelName.contains(labelText) && labelName.contains("")) {

                for (int j = 0; j < ra.size(); j++) {
                    String nameRadio = ra.get(j).findElement(By.tagName("input")).getAttribute("name");

                    if (ra.get(j).getText().contains(radioButtonText) && nameRadio.contains(id2)) {
                        Thread.sleep(1000);
                        ra.get(j).click();
                        break;
                    }
                }
                break;
            }
        }
    }

    private static void isValidation() {
        List<WebElement> isEr = driver.findElements(By.tagName("aria-label"));

        if (isEr.size() > 0) {
            System.out.println("Walidacja występuje.");
        } else {
            System.out.println("Walidacja nie występuje.");
        }
    }

    private static void printValidationMessage() throws InterruptedException {
        List<WebElement> er = driver.findElements(By.xpath("//*[contains(@class, 'error')]"));

        Thread.sleep(2000); // po kliknięciu buttonu czekamy, aż pojawią się walidacje
        for (WebElement a: er) {
            if (!a.getText().equals("")) {
                if (a.getAttribute("aria-label") != null) {
                    System.out.println("Walidacja w polu: " + a.findElement(By.tagName("label")).getText());
                    System.err.println(a.getAttribute("aria-label"));
                }
            }
        }
    }

    public static void waitForloadingOverlayDiv() {
//        Metoda czeka, aż pojawi się i zniknie ekran ładowania (animacja ładowania na stronie)
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("loadingOverlayDiv")));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("loadingOverlayDiv")));
    }

    public static void inputOrTextareaFill(String labelText, String inputText) {
        List<WebElement> list = driver.findElements(By.className("form-group"));
        for (int i = 0; i < list.size(); i++) {
            String tmpId = list.get(i).findElement(By.tagName("label")).getAttribute("for");

            if (list.get(i).getText().contains(labelText)) {

                if (list.get(i).findElement(By.id(tmpId)).getTagName().equals("input")) {
                    List<WebElement> list3 = driver.findElements(By.xpath("//input[contains(@id, '" + tmpId + "')]"));

                    if (list3.size() == 1) {
                        list3.get(0).sendKeys(inputText);
                        break;
                    } else {
                        System.err.println("Lista input jest większa niż 1. Metoda nie zadziała");
                    }
                    break;
                } else if (list.get(i).findElement(By.id(tmpId)).getTagName().equals("textarea")) {
                    List<WebElement> list4 = driver.findElements(By.xpath("//textarea[contains(@id, '" + tmpId + "')]"));

                    if (list4.size() == 1) {
                        list4.get(0).sendKeys(inputText);
                        break;
                    } else {
                        System.err.println("Lista textarea jest większa niż 1. Metoda nie zadziała");
                    }
                    break;
                } else {
                    System.err.println("Nie znalazło ani input ani textarea");
                }
            }
        }
    }

    public static void searchInput(String labelText, String selectInputText) {
        List<WebElement> sp = driver.findElements(By.xpath("//div[contains(@class, 'form-group')]"));

        for (int i = 0; i < sp.size(); i++) {
//            Przypisuje do zmiennej tmpId atrybut ID
            String tmpId = sp.get(i).findElement(By.tagName("label")).getAttribute("for");

            if (sp.get(i).getText().contains(labelText)) {
//                Przeszukuję wszystkie tagi span, które zawierają wcześniej przypisane ID (size() powinien być 1)
                List<WebElement> sp2 = driver.findElements(By.xpath("//span[contains(@id, '" + tmpId + "')]"));

                if (sp2.size() == 1) {
//                    klikam w 1 element
                    sp2.get(0).click();
//                    zmienam na aktywny element (focus jest na input, więc nie trzeba dodatkowo go szukać)
                    driver.switchTo().activeElement().sendKeys(selectInputText);
//                    czekam na widoczność parametru selectInputText w ul->li
                    wait.until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath("//ul[contains(@id, '" + tmpId + "')]//li[contains(text(), '" + selectInputText + "')]")));

//                    Lista elementów w ul->li
                    List<WebElement> sp3 = driver.findElements(By.xpath("//ul[contains(@id, '" + tmpId + "')]//li"));

                    for (int j = 0; j < sp3.size(); j++) {
                        if (sp3.get(j).getText().contains(selectInputText)) {
//                            jeżeli w liście znajdzie się element, który rozwinął się na inpucie, zostanie kliknięty
                            sp3.get(j).click();
                            break;
                        }
                    }
                    break;
                } else {
//                    gdyby lista sp2 byłą większa od 1
                    System.err.println("Lista tag->span z tmpID jest większa od 1.");
                }
            }
        }
    }
}