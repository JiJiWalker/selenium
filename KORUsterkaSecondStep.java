import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.security.Key;
import java.security.KeyStore;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class KORUsterkaSecondStep {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {

        System.setProperty("webdriver.firefox.marionette", "C:\\geckodriver.exe");
        driver = new FirefoxDriver();
        baseUrl = "https://bpms-tst-app1.bpms.inea.ad:9443/ProcessPortal/login.jsp";
//        tst - https://bpms-tst-app1.bpms.inea.ad:9443/ProcessPortal/dashboards/TWP/BPM_WORK?tw.local.view=taskcompletion&tw.local.taskid=368923
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testKORTworzenieUsterki() throws Exception {

        driver.get(baseUrl);

//    Logowanie
        WebElement userName;
        WebElement password;
        WebElement logIn;
        WebElement searchBar;
        WebElement clickCase;

        Thread.sleep(200);
        userName = driver.findElement(By.id("username"));
//    WebDriverWait wait = new WebDriverWait(driver, 10);
//    WebElement el = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
//
        userName.sendKeys("");
        password = driver.findElement(By.id("password"));
        password.sendKeys("");
        logIn = driver.findElement(By.id("log_in"));
        logIn.click();

//        wyszukiwanie etapu
        searchBar = driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_task_list_SearchBar_0_input"));
        searchBar.sendKeys("Klasyfikacja sprawy");
        searchBar.sendKeys(Keys.ENTER);
        searchBar.clear();

//        klikanie etapu
        WebDriverWait wait = new WebDriverWait(driver, 10);
        Thread.sleep(2000);
        WebElement cc = driver.findElements(By.className("bpm-social-task-row-data-subject")).get(0);
        wait.until(ExpectedConditions.visibilityOf(cc));

        System.out.println(cc.getText());
        cc.click();

//        Zmiana iframe
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Wybierz typ sprawy")));
//        Thread.sleep(6000);#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)
//        JavascriptExecutor jse = (JavascriptExecutor)driver;
//        WebElement fra = driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)"));

        Thread.sleep(10500); // <--- TODO zrobić tak, żeby czekało na załadowanie iframe w całości razem z umowami.
        WebElement frame = driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)"));
        driver.switchTo().frame(frame);

//        Wypełnienie formularza
        Select sel = new Select(driver.findElement(By.cssSelector("#combo_div_41")));
        System.out.println(sel.getOptions());
        sel.selectByVisibleText("Usterka");
//        sel.selectByIndex(1); - też UST
//        driver.findElement(By.cssSelector("option[value=\"Usterka\"]")).click();

//        new Select(driver.findElement(By.id("combo_div_2"))).selectByVisibleText("Usterka");
//        driver.findElement(By.cssSelector("option[value=\"Usterka\"]")).click();
//        new Select(driver.findElement(By.cssSelector("#combo_div_41"))).selectByVisibleText("Usterka");
        System.out.println("działa");

//        Dalej
        driver.findElement(By.cssSelector("#div_42_3_1_1_1_1_3_1_1 > div > button")).click();

        System.out.println("Koinec drugiego etapu");

//        #com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1) ** selektor na jutro do 3 etapu
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
