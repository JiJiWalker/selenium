import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class IndexOfIframe {
    public static void main(String[] args) {

        System.setProperty("webdriver.firefox.marionette", "C:\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://dev-bpm85-003.ibpmpro.srv:9443/ProcessPortal/login.jsp");
        driver.findElement(By.id("username")).sendKeys("");
        driver.findElement(By.id("password")).sendKeys("");
        driver.findElement(By.id("log_in")).click();
        driver.findElement(By.id("com_ibm_bpm_social_widgets_task_list_SearchBar_0_input")).click();
        driver.findElement(By.id("com_ibm_bpm_social_widgets_task_list_SearchBar_0_input")).sendKeys("Przygotowanie zgłoszenia");
        driver.findElement(By.id("com_ibm_bpm_social_widgets_task_list_SearchBar_0_input")).sendKeys(Keys.ENTER);
        driver.findElement(By.linkText("Step: Przygotowanie zgłoszenia")).click();

        //////////////////////////////////////////////////////

        int size = driver.findElements(By.tagName("iframe")).size();

        for (int i = 0; i <= size ; i++) {
            driver.switchTo().frame(i);
            int total = driver.findElements(By.xpath("html/body/a/img")).size();
            System.out.println(total);
            driver.switchTo().defaultContent();
        }
    }
}
