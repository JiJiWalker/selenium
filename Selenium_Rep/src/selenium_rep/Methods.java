package selenium_rep;

import java.io.File;
import java.util.ArrayList;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Methods {

    static String[] tablesParam;
    final static String[] xPaths = {"//select[starts-with(@class, 'form-control col-xs-')]",
        "//input[starts-with(@class, 'someDate')]", "//label[starts-with(@class, 'radio')]"};
    final static String claim = "claim()";
    final static String iframeCondition = "javascript";
    final static String logOut = "logOut()";
    final static String selectPage = "selectPage(page + 1, $event)";
    static WebDriver driver;
    static WebDriverWait wait;
    static WebDriverWait longWait;
    String chromeURL;
    static JavascriptExecutor js;

    public Methods(String chromeURL) {
        super();
        System.setProperty("webdriver.chrome.driver", chromeURL);
        Methods.driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 35);
        longWait = new WebDriverWait(driver, 200);
        js = (JavascriptExecutor) driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        Methods.driver = driver;
    }

    public String getChromeDriver() {
        return chromeURL;
    }

    public void setChromeDriver(String chromeURL) {
        this.chromeURL = chromeURL;
    }

    public static void startTest(String url) {
        driver.manage().window().maximize();
        driver.get(url);

    }

    public static void logToPortal(String login, String pass) {
        WebElement userid;
        WebElement password;
        driver.get("https://srvbpmdev10.local.umed.pl:9443/ProcessPortal/login.jsp");

//        driver.get("https://srvbpmdev10.local.umed.pl:9443/ProcessPortal/launchTaskCompletion?taskId=445351");
        // artykuly
        // delegacja
//		driver.get("https://srvbpmdev10.local.umed.pl:9443/ProcessPortal/launchTaskCompletion?taskId=432973");
        driver.manage().window().maximize();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        userid = driver.findElement(By.id("username"));
        userid.sendKeys(login);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
        password = driver.findElement(By.id("password"));
        password.sendKeys(pass);

        driver.findElement(By.cssSelector("#signInForm > a")).click();
    }

    public static void setIframe(int sleep) throws InterruptedException {
        sleep = 1000 * sleep;
        List<WebElement> iframes = driver.findElements(By.tagName("iframe"));
        int i = 0;
        System.out.println(iframes.size());
        for (WebElement webElement : iframes) {
            String iframe = iframes.get(i).getAttribute("ng-attr-title");
            if (iframe != null) {
                if (iframe.equals("{{coachTitle}}")) {
                    WebElement iframeToSwitch = iframes.get(i);

                    driver.switchTo().frame(iframeToSwitch);
                    Thread.sleep(sleep);
                    System.out.println("ustawil iframe 1");

                    List<WebElement> iframesIn = driver.findElements(By.tagName("iframe"));
                    // System.out.println("zagniezdzony: " + iframesIn.get(0).getAttribute("src"));
                    System.out.println("Iframe zagniezdzone: " + iframesIn.size());
                    if (iframesIn.size() >= 1 && !iframesIn.get(0).getAttribute("src").contains(iframeCondition)) {
                        WebElement internalIFrame;
                        internalIFrame = iframesIn.get(0);
                        driver.switchTo().frame(internalIFrame);
                        System.out.println("ustawil iframe 2");
                    }
                    break;
                }
            }
            i++;
        }
    }

    public static void newTask(String taskName, int time) throws InterruptedException {
        time = 1000 * time;
        Thread.sleep(time);
        List<WebElement> processesList = driver.findElements(By.className("menu-link-body"));
        for (int i = 0; i < processesList.size(); i++) {
            System.out.println(processesList.get(i).getText());

            if (processesList.get(i).getText().contains(taskName)) {
                System.out.println(i);
                processesList.get(i).click();
                break;
            }
        }
    }

    public static void pickDate(String labelText, String year, String month, String day) {
        List<WebElement> list = driver.findElements(By.className("form-group"));
        list.addAll(driver.findElements(By.className("control-group")));
        // System.out.println(list.get(8).getText());

        for (WebElement aList : list) {

            if (aList.getText().contains(labelText)) {
                List<WebElement> dateList = driver.findElements(By.xpath("//input[starts-with(@class, 'someDate')]"));

                for (WebElement aDateList : dateList) {
                    String id = aDateList.getAttribute("id");

                    if (aList.findElement(By.tagName("label")).getAttribute("for").equals(id)) {
                        aDateList.sendKeys(year + "-" + month + "-" + day);
                        break;
                    } else {
                        continue;
                    }
                }
                break;
            }
        }
    }

    public static void newTask(String taskName, String snapshotName, int time) throws InterruptedException {
        time = 1000 * time;
        Thread.sleep(time);
        List<WebElement> processesList = driver.findElements(By.className("menu-link-body"));
        for (int i = 0; i < processesList.size(); i++) {
//            System.out.println(processesList.get(i).getText());

            if (processesList.get(i).getText().contains(taskName)) {
//                System.out.println(i);
                List<WebElement> iconList = driver.findElements(By.className("menu-link-icon"));
                iconList.get(i).click();
                break;
            }
        }
        Thread.sleep(1000);
        WebElement txt = driver.findElement(By.linkText(snapshotName));
        if (txt != null) {
            txt.click();
        }
    }

    public static void newTask(String taskName, String snapshotName, String trackName, int time) throws InterruptedException {
        time = 1000 * time;
        Thread.sleep(time);
        List<WebElement> processesList = driver.findElements(By.className("menu-link-body"));
        for (int i = 0; i < processesList.size(); i++) {
            System.out.println(processesList.get(i).getText());

            if (processesList.get(i).getText().contains(taskName)
                    && processesList.get(i).getText().contains(trackName)) {
                System.out.println(i);
                Thread.sleep(1000);
                List<WebElement> iconList = driver.findElements(By.className("menu-link-icon"));
                iconList.get(i).click();
                break;
            } else {
                continue;
            }
        }
        Thread.sleep(2000);
        WebElement txt = driver.findElement(By.linkText(snapshotName));
        if (txt != null) {
            txt.click();
        }
    }

    public static void newTaskWithTrack(String taskName, String trackName, int time) throws InterruptedException {
        time = 1000 * time;
        Thread.sleep(time);
        List<WebElement> processesList = driver.findElements(By.className("menu-link-body"));
        for (int i = 0; i < processesList.size(); i++) {
            System.out.println(processesList.get(i).getText());

            if (processesList.get(i).getText().contains(taskName)
                    && processesList.get(i).getText().contains(trackName)) {
                System.out.println(i);
                processesList.get(i).click();
                break;
            } else {
                continue;
            }
        }
    }

    /*
        Try catch koniecznie w else if dalej w metodzie trzeba dodać
     */
    public static void fillInput(String labelName, String value) throws InterruptedException {
        // wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("control-label")));
        List<WebElement> listOfInputs = driver.findElements(By.className("control-label"));
//        System.out.println("lenght: " + listOfInputs.size());
        //listOfInputs.addAll(driver.findElements(By.xpath("//label[contains(@class, 'control-label')]")));
        boolean done = false;

        for (int i = 0; i < listOfInputs.size(); i++) {
            List<WebElement> spans = listOfInputs.get(i).findElements(By.tagName("span"));
//            System.out.println("Element: " + listOfInputs.get(i).getText() + " spany: " + spans.size());
            if (spans.size() > 0) {
//                System.out.println("element: " + spans.get(0).getAttribute("textContent"));
                if (spans.get(0).getAttribute("textContent").equals(labelName)) {
                    WebElement parent = listOfInputs.get(i).findElement(By.xpath(".."));
                    parent = parent.findElement(By.xpath(".."));
                    if (checkFieldInput(parent)) {
                        try {
//                            System.out.println("parent" + parent.getAttribute("class"));
                            List<WebElement> inputs = parent.findElements(By.tagName("input"));
                            System.out.println("lenght " + inputs.size());
//                            System.out.println("spann " + spans.get(0).getAttribute("textContent"));
//                            System.out.println("id " + inputs.get(0).getAttribute("id") + "classs " + inputs.get(0).getAttribute("class"));
//                            System.out.println("**tag " + inputs.get(0).getAttribute("tagName"));
//                        wait.until(ExpectedConditions.visibilityOf(inputs.get(0)));
                            js.executeScript("arguments[0].click();", inputs.get(0));
                            Thread.sleep(200);
                            inputs.get(0).clear();
                            inputs.get(0).sendKeys(value, Keys.TAB);
                            done = true;
                            break;
                        } catch (ElementNotVisibleException e) {
                        } catch (InvalidElementStateException s) {
                        }
                    } else if (checkFieldTextArea(parent)) {
                        List<WebElement> textareas = parent.findElements(By.tagName("textarea"));
//                        System.out.println("lenght " + textareas.size());
                        js.executeScript("arguments[0].click();", textareas.get(0));
                        Thread.sleep(200);
                        textareas.get(0).clear();
                        textareas.get(0).sendKeys(value, Keys.TAB);
                        done = true;
                        break;
                    }
                }
            } else {
                if (listOfInputs.get(i).getAttribute("textContent").equals(labelName)) {
                    js.executeScript("arguments[0].click();", listOfInputs.get(i));
                    Thread.sleep(200);
                    driver.switchTo().activeElement().clear();
                    driver.switchTo().activeElement().sendKeys(value, Keys.TAB);
//                    System.out.println("Znalazł pole: " + labelName + " po control-label");
                    done = true;
                    break;
                }
            }
        }
        //Dla hidden labels które siedzą w div o klasie jak nizej 
        if (!done) {
            fillHiddenInputFields(labelName, value, -1);
        }
    }

    public static void fillHiddenInputFields(String labelName, String value, int position) throws InterruptedException {
        List<WebElement> hiddenLabels = driver.findElements(By.xpath("//label[contains(@class, 'text controlLabel')]"));
        List<WebElement> hiddenDuplicates = new ArrayList();
        System.out.println("Hidden labels Array lenght: " + hiddenLabels.size());
        // dla posistion -1 nie uwzglednia duplikatow
        if (position == -1) {
            for (WebElement hiddenLabel : hiddenLabels) {
                //                System.out.println("label hidden " + hiddenLabel.getText() + " tag " + hiddenLabel.getTagName());
                System.out.println("innerHTML " + hiddenLabel.getAttribute("innerHTML"));
                if (hiddenLabel.getAttribute("innerHTML").contains(labelName)) {
                    WebElement parent = hiddenLabel.findElement(By.xpath(".."));
                    parent = parent.findElement(By.xpath(".."));
//                    System.out.println("parent class: " + parent.getAttribute("class"));
                    try {
                        parent.findElement(By.tagName("input"));
                    } catch (Exception e) {
                        parent.findElement(By.tagName("textarea"));
                    }

//                    System.out.println("parent : " + parent.getAttribute("class") + ", " + parent.getTagName());
                    //                    parent.click();
                    js.executeScript("arguments[0].click();", parent);
                    Thread.sleep(200);
                    driver.switchTo().activeElement().clear();
                    driver.switchTo().activeElement().sendKeys(value, Keys.TAB);
                    break;
                }
            }
            //tu uwzglednia duplikaty
        } else {
            for (WebElement hiddenLabel : hiddenLabels) {
                //                System.out.println("label hidden " + hiddenLabel.getText() + " tag " + hiddenLabel.getTagName());
//                System.out.println("innerHTML " + hiddenLabel.getAttribute("innerHTML"));
                if (hiddenLabel.getAttribute("innerHTML").contains(labelName)) {
                    WebElement parent = hiddenLabel.findElement(By.xpath(".."));
                    parent = parent.findElement(By.xpath(".."));
//                    System.out.println("parent class: " + parent.getAttribute("class"));
                    try {
                        parent.findElement(By.tagName("input"));
                        hiddenDuplicates.add(parent);
                    } catch (Exception e) {
                        parent.findElement(By.tagName("textarea"));
                        hiddenDuplicates.add(parent);
                    }

//                    System.out.println("parent : " + parent.getAttribute("class") + ", " + parent.getTagName());
                    //                    parent.click();
                }
            }
//            System.out.println("HiddenDuplicates: " + hiddenDuplicates.size());
            if (position <= hiddenDuplicates.size()) {
                js.executeScript("arguments[0].click();", hiddenDuplicates.get(position));
                Thread.sleep(200);
                driver.switchTo().activeElement().clear();
                driver.switchTo().activeElement().sendKeys(value, Keys.TAB);
            } else {
//                System.err.println("Nie ma takiego indeksu duplikatu.");
            }

        }

    }

    // Przeciężona metoda - dodatkowy parametr position. W sytuacji kiedy są takie same 2 pola z tkaimi samymi etykietami np. w tabeli.
    // Tradycyjnie 0 element na liscie jest 1, czyli wywołujac position 0 odnosimy sie do pierwszego elementu ;)
    public static void fillInput(String labelName, String value, int position) throws InterruptedException {
        // wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("control-label")));
        Thread.sleep(3000);
        List<WebElement> duplicates = new ArrayList();
        List<WebElement> listOfInputs = driver.findElements(By.className("control-label"));
//        System.out.println("lenght: " + listOfInputs.size());
        //listOfInputs.addAll(driver.findElements(By.xpath("//label[contains(@class, 'control-label')]")));
        boolean done = false;

        for (int i = 0; i < listOfInputs.size(); i++) {
            List<WebElement> spans = listOfInputs.get(i).findElements(By.tagName("span"));
//            System.out.println("Element: " + listOfInputs.get(i).getText() + " spany: " + spans.size());
            if (spans.size() > 0) {
//                System.out.println("element: " + spans.get(0).getAttribute("textContent"));
                if (spans.get(0).getAttribute("textContent").equals(labelName)) {
                    WebElement parent = listOfInputs.get(i).findElement(By.xpath(".."));
                    parent = parent.findElement(By.xpath(".."));
                    if (checkFieldInput(parent)) {
                        try {
//                            System.out.println("parent" + parent.getAttribute("class"));
                            List<WebElement> inputs = parent.findElements(By.tagName("input"));
//                            System.out.println("lenght " + inputs.size());
//                            System.out.println("spann " + spans.get(0).getAttribute("textContent"));
//                            System.out.println("id " + inputs.get(0).getAttribute("id") + "classs " + inputs.get(0).getAttribute("class"));
//                            System.out.println("**tag " + inputs.get(0).getAttribute("tagName"));
//                        wait.until(ExpectedConditions.visibilityOf(inputs.get(0)));

                            duplicates.add(inputs.get(0));
                            done = true;

                        } catch (ElementNotVisibleException e) {
                        } catch (InvalidElementStateException s) {
                        }
                    } else if (checkFieldTextArea(parent)) {
                        List<WebElement> textareas = parent.findElements(By.tagName("textarea"));
//                        System.out.println("lenght " + textareas.size());
//                        js.executeScript("arguments[0].click();", textareas.get(0));
//                        textareas.get(0).clear();
//                        textareas.get(0).sendKeys(value);
                        duplicates.add(textareas.get(0));
                        done = true;

                    }
                }
            } else {
                if (listOfInputs.get(i).getAttribute("textContent").equals(labelName)) {
                    duplicates.add(listOfInputs.get(i));
//                    js.executeScript("arguments[0].click();", listOfInputs.get(i));
//                    driver.switchTo().activeElement().clear();
//                    driver.switchTo().activeElement().sendKeys(value);
//                    System.out.println("Znalazł pole: " + labelName + " po control-label");
                    done = true;

                }
            }
        }

        //Dla hidden labels które siedzą w div o klasie jak nizej 
        if (!done) {
            fillHiddenInputFields(labelName, value, position);
        } else if (done) {
//            System.out.println("Duplicates: " + duplicates.size());
            for (int i = 0; i < duplicates.size(); i++) {
                System.out.println("duplikat " + duplicates.get(position).getText());
                if (position <= duplicates.size()) {
                    js.executeScript("arguments[0].click();", duplicates.get(position));
                    Thread.sleep(200);
                    driver.switchTo().activeElement().clear();
                    driver.switchTo().activeElement().sendKeys(value, Keys.TAB);
                } else {
//                    System.err.println("Nie ma duplikatu o takim indeksie");
                }
            }
        }

    }

    public static void selectValueFromDropDown(String labelText, String valueText) throws InterruptedException {
        Thread.sleep(200);
        List<WebElement> list = driver.findElements(By.className("form-group"));
//        System.out.println("size: " + list.size());
        list.addAll(driver.findElements(By.className("control-group")));
//        System.out.println("size: " + list.size());
        // System.out.println(list.size());
        // System.out.println(list.get(8).getText());
        // System.out.println(list.get(8).findElement(By.tagName("label")));
        // System.out.println(list.get(8).findElement(By.tagName("label")).getAttribute("for"));

        // System.out.println(list.get(8).getText());
        MainLoop:
        for (int i = 0; i < list.size(); i++) {

//            System.out.println("element " + "i " + list.get(i).getText());
            if (list.get(i).getText().contains(labelText)) {
                // List<WebElement> lista4 =
                // driver.findElements(By.cssSelector("select[class='form-control
                // col-xs-12']"));
                List<WebElement> lista24 = driver
                        .findElements(By.xpath("//select[starts-with(@class, 'form-control col-xs-')]"));
//                System.out.println("lenght: " + lista24.size());
                lista24.addAll(list.get(i).findElements(By.className("control-label")));
//                System.out.println("lenght: " + lista24.size());
                for (int j = 0; j < lista24.size(); j++) {
//                    System.out.println("-----");
//                    System.out.println("element select: " + lista24.get(j).getText());
                    String id = lista24.get(j).getAttribute("id");
                    if (lista24.get(j).getText().equals(labelText)) {
                        WebElement parent = lista24.get(j).findElement(By.xpath(".."));
//                        System.out.println("parent: " + parent.getAttribute("class"));
                        Select sel = new Select(parent.findElement(By.tagName("select")));
                        sel.selectByVisibleText(valueText);
                        break MainLoop;
                    }
                    // zwraca listÄ™ value/options oraz id z rozwijanych select
                    // System.out.println(lista24.get(j).getText());
                    // System.out.println(lista24.get(j).getAttribute("id"));

                    // lista24.get(j).getText().contains(valueText) &&
                    // list.get(i).getText().contains(labelText) &&
                    // list.get(i).getText().contains(valueText)
                    if (lista24.get(j).getText().contains(valueText)
                            && list.get(i).findElement(By.tagName("label")).getAttribute("for").equals(id)) {
                        Select sel = new Select(driver.findElement(By.id(id)));
                        sel.selectByVisibleText(valueText);
                        break MainLoop;
                    } else {
                        continue;
                    }
                }
                break;
            }
        }
    }

    // Przeciążona metoda, wzbogacona o parametr position na wypadek duplikatów np te same pola w tabeli 
    // Tak jak w fillinput podawany jest indeks czyli zaczynami od zera numeracje ;)
    public static void selectValueFromDropDown(String labelText, String valueText, int position) throws InterruptedException {
        List<Select> duplicates = new ArrayList();
        List<WebElement> list = driver.findElements(By.className("form-group"));
//        System.out.println("size: " + list.size());
        list.addAll(driver.findElements(By.className("control-group")));
//        System.out.println("size: " + list.size());
        // System.out.println(list.size());
        // System.out.println(list.get(8).getText());
        // System.out.println(list.get(8).findElement(By.tagName("label")));
        // System.out.println(list.get(8).findElement(By.tagName("label")).getAttribute("for"));

        // System.out.println(list.get(8).getText());
        MainLoop:
        for (int i = 0; i < list.size(); i++) {
//            System.out.println("element " + "i " + list.get(i).getText());
            if (list.get(i).getText().contains(labelText)) {
                // List<WebElement> lista4 =
                // driver.findElements(By.cssSelector("select[class='form-control
                // col-xs-12']"));
                List<WebElement> lista24 = driver
                        .findElements(By.xpath("//select[starts-with(@class, 'form-control col-xs-')]"));
//                System.out.println("lenght: " + lista24.size());
                lista24.addAll(list.get(i).findElements(By.className("control-label")));
//                System.out.println("lenght: " + lista24.size());
                for (int j = 0; j < lista24.size(); j++) {
//                    System.out.println("-----");
//                    System.out.println("element select: " + lista24.get(j).getText());
                    String id = lista24.get(j).getAttribute("id");
                    if (lista24.get(j).getText().equals(labelText)) {
                        WebElement parent = lista24.get(j).findElement(By.xpath(".."));
//                        System.out.println("parent: " + parent.getAttribute("class"));
                        Select sel = new Select(parent.findElement(By.tagName("select")));
                        duplicates.add(sel);
                        //sel.selectByVisibleText(valueText);
                        //break MainLoop;
                    }
                    // zwraca listÄ™ value/options oraz id z rozwijanych select
                    // System.out.println(lista24.get(j).getText());
                    // System.out.println(lista24.get(j).getAttribute("id"));

                    // lista24.get(j).getText().contains(valueText) &&
                    // list.get(i).getText().contains(labelText) &&
                    // list.get(i).getText().contains(valueText)
                    if (lista24.get(j).getText().contains(valueText)
                            && list.get(i).findElement(By.tagName("label")).getAttribute("for").equals(id)) {
                        Select sel = new Select(driver.findElement(By.id(id)));
                        duplicates.add(sel);
//                        sel.selectByVisibleText(valueText);
//                        break MainLoop;
                    }
                }
            }
        }
        if (position <= duplicates.size()) {
            duplicates.get(position).selectByVisibleText(valueText);
        } else {
//            System.err.println("Nie ma duplikatu (select) o podanym indeksie.");
        }

    }

    public static void logOutAndLogIn(String login, String pass) throws InterruptedException {
        Thread.sleep(2000);
        List<WebElement> logOuts = driver.findElements(By.tagName("a"));
        logOuts.size();
        for (int i = 0; i < logOuts.size(); i++) {
            if (logOuts.get(i).getAttribute("ng-click").equals(Methods.logOut)) {
                js.executeScript("arguments[0].click();", logOuts.get(i));
                break;
            }
        }
        Thread.sleep(2000);
        logToPortal(login, pass);
    }

    public static void searchTaskAfter(String taskName, int seconds) throws InterruptedException {
        driver.switchTo().defaultContent();

        seconds = 1000 * seconds;
        Thread.sleep(seconds);
        WebElement searchFiltr;
        searchFiltr = driver.findElement(By.cssSelector("input[ng-model='searchFilter']"));
        js.executeScript("arguments[0].click();", searchFiltr);
        searchFiltr.sendKeys(taskName, Keys.ENTER);

        Thread.sleep(2500);

        List<WebElement> btns = driver.findElements(By.tagName("a"));
        for (int i = 0; i < btns.size(); i++) {
            // System.out.println("atr " + btns.get(i).getAttribute("ng-click"));
            if (btns.get(i).getAttribute("ng-click") != null
                    && btns.get(i).getAttribute("ng-click").equals("selectPage(page + 1, $event)")) {
                String numbers = driver.findElement(By.className("ui-grid-pager-count")).findElement(By.tagName("span"))
                        .getAttribute("textContent");
//                System.out.println("ciag " + numbers);

                List<Double> lista1 = new ArrayList<Double>();
                Pattern p = Pattern.compile("\\d+");
                Matcher m = p.matcher(numbers);
                while (m.find()) {
                    lista1.add(Double.parseDouble(m.group()));
                }

                double clickTimes = lista1.get(lista1.size() - 1) / lista1.get(lista1.size() - 2);
                int clickTimesInt;
                if (clickTimes > 1) {
                    clickTimesInt = (int) Math.floor(clickTimes);
                    for (int j = 0; j < clickTimesInt; j++) {
//                        System.out.println("int" + clickTimesInt);
                        js.executeScript("arguments[0].click();", btns.get(i));
                        Thread.sleep(200);
                    }
                }

                break;
            }
        }
        List<WebElement> taskInfos = driver.findElements(By.className("task-info"));
//        System.out.println("długość : " + taskInfos.size());
        Thread.sleep(3500);
        // for (WebElement webElement : taskInfos) {
        // System.out.println("data: " + webElement.getAttribute("data-piid"));
        // }
        WebElement lastTask;
        int i = 0;
        do {
            i++;
            try {
                lastTask = taskInfos.get(taskInfos.size() - 1).findElement(By.className("name")).findElement(By.tagName("a"));
                js.executeScript("arguments[0].click();", lastTask);
                break;
            } catch (IndexOutOfBoundsException e) {
//                System.err.println("Nie podziałało problem z długością tablicy ;/, długość tablicy: " + taskInfos.size());
            }
            Thread.sleep(600);
        } while (i <= 10);
        if (taskInfos.isEmpty()) {
//            System.err.println("Nie ma takiego zadania jak, koniec pracy!!");
//            System.exit(1);
        }

        Thread.sleep(1500);
        boolean check = existsElement("modal-content");
        if (check) {
            WebElement modal = driver.findElement(By.className("modal-content"));
            List<WebElement> modalButtons = modal.findElements(By.tagName("button"));
            clickLooop:
            for (WebElement webElement : modalButtons) {
                if (webElement.getAttribute("ng-click").equals(claim)) {
                    webElement.click();
                    break clickLooop;
                }
            }
        } else {
//            System.out.println("nie wszedl");
        }

    }

    public static void searchTaskByInstanceId(String instanceId) throws InterruptedException {
        Thread.sleep(8000);
        WebElement element;
        WebElement elementParent;
        if (driver.getPageSource().contains(instanceId)) {
            element = driver.findElement(By.partialLinkText(instanceId));
            elementParent = element.findElement(By.xpath("./.."));
            elementParent = elementParent.findElement(By.xpath("./.."));
            elementParent = elementParent.findElement(By.className("name"));
            elementParent = elementParent.findElement(By.tagName("a"));
            // System.out.println("el " + elementParent.getAttribute("class"));
            Thread.sleep(200);
            js.executeScript("arguments[0].click();", elementParent);
            Thread.sleep(1500);
            boolean check = existsElement("modal-content");
            if (check) {
                WebElement modal = driver.findElement(By.className("modal-content"));
                List<WebElement> modalButtons = modal.findElements(By.tagName("button"));
                clickLooop:
                for (WebElement webElement : modalButtons) {
                    if (webElement.getAttribute("ng-click").equals(claim)) {
                        webElement.click();
                        break clickLooop;
                    }
                }
            }
//            System.out.println("kilknal od razu");
        } else {
            List<WebElement> btns = driver.findElements(By.tagName("a"));
            for (int i = 0; i < btns.size(); i++) {
                // System.out.println("atr " + btns.get(i).getAttribute("ng-click"));
                if (btns.get(i).getAttribute("ng-click") != null
                        && btns.get(i).getAttribute("ng-click").equals("selectPage(page + 1, $event)")) {
                    String numbers = driver.findElement(By.cssSelector(
                            "#div_1_2_1_2_2 > div > div.task-container > div > div.task-list-footer > div > div.ui-grid-pager-count-container > div > span"))
                            .getAttribute("textContent");
//                    System.out.println("ciag " + numbers);

                    List<Double> lista1 = new ArrayList<Double>();
                    Pattern p = Pattern.compile("\\d+");
                    Matcher m = p.matcher(numbers);
                    while (m.find()) {
                        lista1.add(Double.parseDouble(m.group()));
                    }

                    double clickTimes = lista1.get(lista1.size() - 1) / lista1.get(lista1.size() - 2);
                    int clickTimesInt;
                    if (clickTimes > 1) {
                        clickTimesInt = (int) Math.floor(clickTimes);
                        clickLoop:
                        for (int j = 0; j < clickTimesInt; j++) {
                            // System.out.println("int" + clickTimesInt);
                            js.executeScript("arguments[0].click();", btns.get(i));
                            Thread.sleep(3000);
                            if (driver.getPageSource().contains(instanceId)) {
                                // Thread.sleep(2000);
                                element = driver.findElement(By.partialLinkText(instanceId));
                                elementParent = element.findElement(By.xpath("./.."));
                                elementParent = elementParent.findElement(By.xpath("./.."));
                                elementParent = elementParent.findElement(By.className("name"));
                                elementParent = elementParent.findElement(By.tagName("a"));
                                Thread.sleep(200);
                                js.executeScript("arguments[0].click();", elementParent);
                                boolean check = existsElement("modal-content");
                                if (check) {
                                    WebElement modal = driver.findElement(By.className("modal-content"));
                                    List<WebElement> modalButtons = modal.findElements(By.tagName("button"));
                                    clickLooop:
                                    for (WebElement webElement : modalButtons) {
                                        if (webElement.getAttribute("ng-click").equals(claim)) {
                                            webElement.click();
                                            break clickLooop;
                                        }
                                    }
                                }
//                                System.out.println("kilknal potem");
                                break clickLoop;
                            }
                            Thread.sleep(200);
                        }
                    } else {
//                        System.out.println("Nie znalazĹ‚o zadania o podanej instancji");
                    }
                }
            }
        }
    }

    // StrumieĹ„ czeka aĹĽ strona siÄ™ zaĹ‚aduje (NIE IFRAME TYLKO STRONA)
    public static void waitForLoad() {
        new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) wd -> ((JavascriptExecutor) wd)
                .executeScript("return document.readyState").equals("complete"));
    }

    public static boolean existsElement(String selector) {
        try {
            driver.findElement(By.className(selector));
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }

    public static WebElement findTable(String ths) throws InterruptedException {
        String[] th;
        WebElement tableResult = null;
        th = ths.split(";");
        Thread.sleep(2500);
        List<WebElement> allTables = driver.findElements(By.tagName("table"));
        int i = 0;
        int j = 0;
//        System.out.println("ile tabel " + allTables.size());
        for (WebElement webElement : allTables) {
            Thread.sleep(400);
            List<WebElement> allTh = webElement.findElements(By.tagName("th"));
            checkTh:
            for (WebElement webElement2 : allTh) {
                if (allTh.size() == th.length) {
                    String content = webElement2.getAttribute("textContent");
                    if (content.equals(th[j])) {
                        j++;
                        if (allTh.size() == j) {
                            System.out.println("znalazĹ‚ tabele");
                            tableResult = webElement;
                            j = 0;
                        }

                    } else {
                        j = 0;
                        break checkTh;
                    }
                } else {
                    break;
                }
            }
            i++;
        }
        return tableResult;
    }

    // not work
    public static void fillTable(String ths, int row, String params) throws InterruptedException {
        tablesParam = new String[0];
        tablesParam = params.split(";");
//        System.out.println("Tables params lenght" + tablesParam.length);

        int jIt = 0;
        int iIt = 0;
        WebElement foundTable = findTable(ths);
//        System.out.println("Znaleziona tabela: " + foundTable.findElement(By.tagName("th")).getText());

        List<WebElement> listOfRows = foundTable.findElement(By.tagName("tbody")).findElements(By.tagName("tr"));
//        System.out.println("Tr: " + listOfRows.size());
        for (WebElement webElement : listOfRows) {

            List<WebElement> listOfCells = listOfRows.get(row).findElements(By.tagName("td"));
            System.out.println("Cells: " + listOfRows.get(row).findElements(By.tagName("td")).size());
            for (WebElement webElement2 : listOfCells) {
//                System.out.println("Jestem w cell");

                List<WebElement> listOfForms = webElement2.findElements(By.className("control-label"));
//                System.out.println("IloĹ›Ä‡ cotrol-label: " + listOfForms.size());
                jIt = 1;
                Filling:
                for (int i = 0; i < tablesParam.length; i += 2) {
                    for (WebElement webElement3 : listOfForms) {
                        Thread.sleep(300);
                        WebElement parent;
                        parent = webElement3.findElement(By.xpath("./.."));
                        parent = parent.findElement(By.xpath("./.."));
//                        System.out.println("class name parent : " + parent.getAttribute("class"));
//                        System.out.println("Label: " + webElement3.getText());
//                        System.out.println("i: " + i);
//                        System.out.println("iIt: " + iIt);
//                        System.out.println("jIt: " + jIt);

                        if (webElement3.getText().equals(tablesParam[i])) {
                            if (checkedWhatField(parent, "input", jIt)) {
                                System.out.println("Jest to input");
                            } else if (checkedWhatField(parent, "select", jIt)) {
                                System.out.println("jest to select!");
                            } else if (checkedWhatField(parent, "radio", jIt)) {
                                System.out.println("jest to radio!");
                            } else if (checkedWhatField(parent, "textarea", jIt)) {
                                System.out.println("jest to textarea!");
                            }
                            break;
                        }
                    }
                    jIt += 2;
                }
            }
            break;
        }

    }

    // not work
    public static boolean checkedWhatField(WebElement field, String fieldType, int it) {
        boolean status = false;

        switch (fieldType) {
            case "input":
                status = checkFieldInput(field, it);
                break;
            case "select":
                status = checkFieldSelect(field, xPaths[0], it);
                break;
            case "radio":
                status = checkFieldRadio(field, xPaths[2], it);
            // case "dataPicker":
            // status = false;
            // break;
            case "textarea":
                status = checkFieldTextArea(field, it);
                break;
            case "doc":
                status = false;
                break;
        }
        return status;
    }

    public static boolean checkFieldSelect(WebElement field, String xpathExpression, int it) {
        try {
            // field.findElement(By.xpath(xpathExpression));

            Select select = new Select(field.findElement(By.tagName("select")));
            select.selectByVisibleText(tablesParam[it]);

            return true;
        } catch (NoSuchElementException e) {

            return false;
        }
    }

    // public static boolean checkFieldDate(WebElement field, String
    // xpathExpression, int it) {
    // try {
    // field.findElement(By.xpath(xpathExpression));
    // WebElement dataPick =field.findElement(By.tagName("input"));
    // dataPick.findElement(By.xpath(xPaths[1]));
    //
    // return true;
    // } catch (NoSuchElementException e) {
    //
    // return false;
    // }
    // }
    public static boolean checkFieldRadio(WebElement field, String xpathExpression, int it) {
        try {
            List<WebElement> r = driver.findElements(By.xpath("//label[starts-with(@class, 'radio')]"));

            for (int i = 0; i < r.size(); i++) {
                // System.out.println(r.get(i).getText());
                if (r.get(i).getText().contains(tablesParam[it])) {
//                    System.out.println("label" + tablesParam[it]);
                    js.executeScript("arguments[0].click();", r.get(i));
                }
            }
            return true;
        } catch (NoSuchElementException e) {

            return false;
        }
    }

    // not work
    public static boolean checkFieldInput(WebElement field, int it) {
        try {

            WebElement input = field.findElement(By.tagName("input"));
            if (input.getAttribute("type").equals("text")) {
                js.executeScript("arguments[0].click();", input);
                input.sendKeys(tablesParam[it]);
                return true;
            } else {
                return false;
            }

            // driver.switchTo().activeElement().sendKeys("test");
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public static boolean checkFieldInput(WebElement field) {
        try {

            field.findElement(By.tagName("input"));

            return true;

        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public static boolean checkFieldTextArea(WebElement field, int it) {
        try {
            WebElement input = field.findElement(By.tagName("textarea"));
            input.click();
            input.sendKeys(tablesParam[it]);
            return true;

        } catch (NoSuchElementException e) {
            // System.out.println("Nie znalazĹ‚ elementu !!!");
            return false;
        }
    }

    public static boolean checkFieldTextArea(WebElement field) {
        try {
            field.findElement(By.tagName("textarea"));
            return true;

        } catch (NoSuchElementException e) {
            // System.out.println("Nie znalazĹ‚ elementu !!!");
            return false;
        }
    }

    public static void documentSend(String docPath, String fileNameDotFormat) throws InterruptedException {

        // System.out.println(docPath + "\\" + fileNameDotFormat);
        List<WebElement> upload = driver.findElements(By.xpath("//div[contains(@class, 'upload-')]//input"));
        if (upload.size() > 0) {
            for (int i = 0; i < upload.size(); i++) {
                // System.out.println(upload.get(i).getAttribute("name"));
                // System.out.println(upload.get(i).getTagName());

                if (upload.get(i).isEnabled()) {
                    File file = new File(docPath);
                    String getFilePath = file.getAbsolutePath();
                    upload.get(i).sendKeys(getFilePath + "\\" + fileNameDotFormat);
                }
            }
        } else {
            buttonClickByText("Dodaj dokument", 1);
            modalButtonClick("Wybierz plik");
            driver.findElement(By.name("fileContent")).sendKeys(docPath + "\\" + fileNameDotFormat);
            modalButtonClick("OK");
            Thread.sleep(1200);
        }

    }

    public static void buttonClickByText(String textOnButton, int time) throws InterruptedException {
        time = 1000 * time;
        Thread.sleep(time);
        List<WebElement> list = driver.findElements(By.xpath("//*[contains(@class, 'btn')]"));
//	        zawÄ™ĹĽa poszukiwania, ale nie jest niezbÄ™dny.
//	        List<WebElement> list2 = driver.findElements(By.xpath("//div[contains(@class, documentList)]//button[contains(@class, 'btn')]"));

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getText() != null && !list.get(i).getText().equals("")) {
                if (list.get(i).getText().equals(textOnButton) || list.get(i).getText().equals(" " + textOnButton)) {
                    try {
                        list.get(i).click();
                    } catch (WebDriverException e) {
//                        System.err.println("Przycisku nie da siÄ™ obsĹ‚uĹĽyÄ‡ klikniÄ™ciem. Wciskam js'em");
                        js.executeScript("arguments[0].click();", list.get(i));
//                        list.get(i).sendKeys(Keys.ENTER);
                    }
                    break;
                }
            }
        }
        Thread.sleep(time);
    }

    public static void modalButtonClick(String textOnButton) {
        List<WebElement> modal = driver
                .findElements(By.xpath("//*[contains(@class, 'modal')]//*[contains(@class, 'btn')]"));
        for (int i = 0; i < modal.size(); i++) {
            // System.out.println(modal.get(i).getTagName());
            // System.out.println(modal.get(i).getAttribute("value"));

            if (modal.get(i).getText().contains(textOnButton)) {
                // pozostaĹ‚a obsĹ‚uga opcji, kiedy element jest niewidoczny || nie da siÄ™ jeszcze
                // w niego kliknÄ…Ä‡
                modal.get(i).click();
            } else if (modal.get(i).getTagName().contains("input")
                    && modal.get(i).getAttribute("value").contains(textOnButton)) {
                modal.get(i).sendKeys(Keys.ENTER);
            } else {
                continue;
            }
        }
    }

    public static void radioButtonClickByText(String radioButtonLabelText) throws InterruptedException {
        List<WebElement> r = driver.findElements(By.xpath("//label[starts-with(@class, 'radio')]"));

        for (int i = 0; i < r.size(); i++) {
//            System.out.println(r.get(i).getText());
            if (r.get(i).getText().contains(radioButtonLabelText)) {
                Thread.sleep(600);
                js.executeScript("arguments[0].click();", r.get(i));
                break;
            }
        }
    }

    public static void radioButtonClickInTabel(String labelText, String radioButtonText) throws InterruptedException {

        List<WebElement> ra = driver.findElements(By.xpath("//div[starts-with(@class, 'form-group')]//*[contains(@class, 'radio')]"));
        List<WebElement> ca2 = driver.findElements(By.xpath("//div[contains(@class, 'form')]//label[contains(@class, 'control')]"));

        for (int i = 0; i < ca2.size(); i++) {
            System.out.println(ca2.get(i).getText()); //zwraca nazwę labela
            System.out.println(ca2.get(i).getAttribute("id")); // zwraca ID labela
            String labelName = ca2.get(i).getText();
            String idLabelNumber = ca2.get(i).getAttribute("id");
            String id2 = idLabelNumber.replace("label_", "");

            if (labelName.contains(labelText) && labelName.contains("")) {

                for (int j = 0; j < ra.size(); j++) {
                    System.out.println(ra.get(j).getText());
                    System.out.println(ra.get(j).getAttribute("name"));
                    String nameRadio = ra.get(j).findElement(By.tagName("input")).getAttribute("name");
                    System.out.println(nameRadio);

                    if (ra.get(j).getText().contains(radioButtonText) && nameRadio.contains(id2)) {
                        Thread.sleep(1000);
                        js.executeScript("arguments[0].click();", ra.get(j));
                        System.out.println("Kliknięto radiobutton");
                        break;
                    }
                }
                break;
            }
        }
    }

    public static void radioByDivClass(String label, String value, int time) throws InterruptedException {
        value = value.trim();
        label = label.trim();
        time = 1000 * time;
        Thread.sleep(time);
        List<WebElement> divs = driver.findElements(By.xpath("//div[contains(@class, 'control-label')]"));
        boolean spr = false;
        RadioFill:
        for (WebElement div : divs) {
            if (div.getText().trim().equals(label)) {
                WebElement parent = div.findElement(By.xpath(".."));
                List<WebElement> radios = parent.findElements(By.tagName("label"));
                System.out.println("lenght: " + radios.size());
                for (WebElement radio : radios) {
                    System.out.println("radio text: " + radio.getText());
                    if (radio.getText().trim().equals(value)) {
                        js.executeScript("arguments[0].click();", radio);
                        spr = true;
                        break RadioFill;
                    }
                }
            }
        }
        if (!spr) {
            radioButtonClickInTabel(label, value);
        }

    }

    public static void checkBoxFill(String label, String value) {
        List<WebElement> controls = driver.findElements(By.xpath("//div[starts-with(@class,'control-label')]"));
//        System.out.println("Teekst: " + controls.get(0).getText());
//        System.out.println("checkboxes: " + controls.size());
        for (int i = 0; i < controls.size(); i++) {
            if (controls.get(i).getText().equals(label)) {
                WebElement parent = controls.get(i).findElement(By.xpath(".."));
                List<WebElement> inputs = parent.findElements(By.xpath("//input[@type='checkbox']"));
//                System.out.println("inputek: " + inputs.get(0).getAttribute("nameval"));
                for (int j = 0; j < inputs.size(); j++) {
                    if (inputs.get(j).getText().equals(value) || inputs.get(j).getAttribute("nameval").equals(value)) {
                        inputs.get(j).click();
                        break;
                    }
                }
            }
        }
    }

    public static boolean checkSpan(WebElement element) {
        try {
            element.findElement(By.tagName("span"));
            return true;
        } catch (NoSuchElementException e) {
            // TODO: handle exception
            return false;
        }
    }

    public static void clickPositionOfButtons(String textOnButton, int position) {

        List<WebElement> list = driver.findElements(By.xpath("//*[contains(@class, 'btn')]"));
//	        zawÄ™ĹĽa poszukiwania, ale nie jest niezbÄ™dny.
//	        List<WebElement> list2 = driver.findElements(By.xpath("//div[contains(@class, documentList)]//button[contains(@class, 'btn')]"));
        List<WebElement> theSameButtons = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getText() != null && !list.get(i).getText().equals("")) {
                if (list.get(i).getText().equals(textOnButton) || list.get(i).getText().equals(" " + textOnButton)) {
                    theSameButtons.add(theSameButtons.size(), list.get(i));
                }
            }
        }
//        System.out.println("Amount of the same buttons: " + theSameButtons.size());
        if (position <= theSameButtons.size()) {
            js.executeScript("arguments[0].click();", theSameButtons.get(position));
            //theSameButtons.get(position).click();
        } else {
//            System.err.println("Nie istnieje przycisk o podanej pozycji");
        }
    }

    public static void inputAndTextAreaWithPlaceHolder(String placeholder, String value) throws InterruptedException {
        List<WebElement> inputs = driver.findElements(By.tagName("input"));
        List<WebElement> textareas = driver.findElements(By.tagName("textarea"));
//        System.out.println("lenght textareas: " + textareas.size());
//        System.out.println("lenght inputs: " + inputs.size());
        Thread.sleep(600);
        for (int i = 0; i < textareas.size(); i++) {
            System.out.println("place: " + textareas.get(i).getAttribute("placeholder"));
            if (textareas.get(i).getAttribute("placeholder").equals(placeholder)) {
                textareas.get(i).click();
                textareas.get(i).clear();
                textareas.get(i).sendKeys(value, Keys.ENTER);
                break;
            } else if (inputs.get(i).getAttribute("placeholder").equals(placeholder)) {
                inputs.get(i).click();
                inputs.get(i).clear();
                inputs.get(i).sendKeys(value, Keys.ENTER);
                break;
            }
        }
    }

    public static void inputAndSelectField(String label, String value) throws InterruptedException {
        Thread.sleep(3000);
        List<WebElement> listOfFields = driver.findElements(By.xpath("//span[@class='select2-selection__rendered']"));

        for (int i = 0; i < listOfFields.size(); i++) {
            WebElement parent = listOfFields.get(i).findElement(By.xpath(".."));
            parent = parent.findElement(By.xpath(".."));
            parent = parent.findElement(By.xpath(".."));
            parent = parent.findElement(By.xpath(".."));
            parent = parent.findElement(By.xpath(".."));
            WebElement lab = parent.findElement(By.className("control-label"));

            if (lab.getText().equals(label)) {
                listOfFields.get(i).click();
                driver.switchTo().activeElement().sendKeys(value);
                Thread.sleep(650);
                driver.switchTo().activeElement().sendKeys(Keys.ENTER);
                break;
            }
        }
    }

    public static void fillFieldByHeaderSection(String sectionName, String value) {
        List<WebElement> headerSections = driver.findElements(By.className("collapseHeaderSection"));
//        System.out.println("Sections size: " + headerSections.size());
        for (WebElement headerSection : headerSections) {
            if (headerSection.getAttribute("innerHTML").equals(sectionName)) {
//                System.out.println("header " + headerSection.getAttribute("innerHTML"));
                WebElement parent = headerSection.findElement(By.xpath(".."));
                try {
                    parent = parent.findElement(By.xpath(".."));
                    parent = parent.findElement(By.xpath(".."));
                    WebElement textarea = parent.findElement(By.className("control-group"));
                    WebElement it = textarea.findElement(By.tagName("textarea"));
                    js.executeScript("arguments[0].click();", it);
                    it.clear();
                    it.sendKeys(value);
                } catch (Exception e) {
                    WebElement textarea = parent.findElement(By.className("form-group"));
                    WebElement it = textarea.findElement(By.tagName("textarea"));
                    js.executeScript("arguments[0].click();", it);
                    it.clear();
                    it.sendKeys(value);
                }
            }
        }
    }
//    Metoda działa jak jest datePicker , uwzględnia dni które są disabled

    public static void pickDateByIcon(String label, String year, String month, String day) throws InterruptedException {
        List<WebElement> list = driver.findElements(By.className("control-label"));
        for (WebElement webElement : list) {
            if (webElement.getText().equals(label)) {
                WebElement parent = webElement.findElement(By.xpath(".."));
                parent = parent.findElement(By.className("icon-calendar"));
                //parent.click();
                js.executeScript("arguments[0].click();", parent);

                List<WebElement> listOfDatePickers = driver.findElements(By.xpath("//div[@class='bootstrap-datetimepicker-widget dropdown-menu']"));
                WebElement yearPick = null;
                for (WebElement listOfDatePicker : listOfDatePickers) {
                    if (listOfDatePicker.getAttribute("style").contains("block")) {
                        System.out.println("Ustawil yearPick");
                        yearPick = listOfDatePicker.findElement(By.tagName("table"));
                        break;
                    }
                }

                Thread.sleep(200);

                List<WebElement> ths = yearPick.findElements(By.tagName("th"));
                WebElement switcher = null;
                WebElement previous = null;
                WebElement next = null;

                for (WebElement th : ths) {
                    if (th.getAttribute("class").equals("prev")) {
                        previous = th;
                    } else if (th.getAttribute("class").equals("next")) {
                        next = th;
                    } else if (th.getAttribute("class").equals("switch")) {
                        switcher = th;
                    }
                }
//                System.out.println("breakpoint");
                boolean spr = false;
                do {

//                    previous.click();
                    System.out.println("switcher.getText() - " + switcher.getAttribute("innerHTML"));
                    if ((switcher.getAttribute("innerHTML").contains(year)) && (switcher.getAttribute("innerHTML").contains(month))) {
                        spr = true;
                    } else {
                        js.executeScript("arguments[0].click();", previous);
                    }

                } while (!spr);
//                System.out.println("Yup");

//                System.out.println("yearPick " + yearPick.getAttribute("class"));
                List<WebElement> td = yearPick.findElements(By.tagName("td"));
//                System.out.println("td: " + td.size());
                for (WebElement webElement1 : td) {
//                    System.out.println("day: " + webElement1.getAttribute("class") + " : " + webElement1.getText());
                    if (webElement1.getText().equals(day) && !webElement1.getAttribute("class").equals("day disabled")) {
                        //webElement1.click();
//                        System.out.println("webelement: " + webElement1.getAttribute("class") + webElement1.getTagName());
                        js.executeScript("arguments[0].click();", webElement1);
                        break;
                    }
                }
            }
        }
    }

    public static void waitForloadingOverlayDiv() {
//        Metoda czeka, aż pojawi się i zniknie ekran ładowania (animacja ładowania na stronie)
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("loadingOverlayDiv")));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("loadingOverlayDiv")));
        //TEST TEST TEST
    }
}
