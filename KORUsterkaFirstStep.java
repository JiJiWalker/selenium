

import java.util.List;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class KORUsterkaFirstStep {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {

    System.setProperty("webdriver.firefox.marionette", "C:\\geckodriver.exe");
    driver = new FirefoxDriver();
    baseUrl = "https://bpms-tst-app1.bpms.inea.ad:9443/ProcessPortal/login.jsp";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testKORTworzenieUsterki() throws Exception {

//    Base URL
    driver.get(baseUrl);

//    Logowanie
    WebElement userName;
    WebElement password;
    WebElement logIn;

    Thread.sleep(200); // TODO dopisać wait.until
    userName = driver.findElement(By.id("username"));
//    WebDriverWait wait = new WebDriverWait(driver, 10);
//    WebElement el = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
//
    userName.sendKeys("");
    password = driver.findElement(By.id("password"));
    password.sendKeys("");
    logIn = driver.findElement(By.id("log_in"));
    logIn.click();

//    Nowa instancja Procesu Obsługi sprawy na DEV
//    driver.findElement(By.linkText("Proces obsługi sprawy (Main (Rozwój FSM))")).click();
//    Nowa instancja TST
//    driver.findElement(By.linkText("Proces obsługi sprawy")).click();
//    Gotowa instancja
    driver.get("https://bpms-tst-app1.bpms.inea.ad:9443/ProcessPortal/dashboards/TWP/BPM_WORK?tw.local.view=taskcompletion&tw.local.taskid=373759");

//    Zmiana frame
    WebDriverWait wait = new WebDriverWait(driver, 40);
    WebElement iframe = driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)"));
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)")));
    driver.switchTo().frame(iframe);

//    Uzupełnienie danych klienta

    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("input_div_1_1_2_1_1_1_1"))).sendKeys("Jacek");
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("input_div_1_1_2_1_1_1_2"))).sendKeys("Mrózek");

//    Wyszukanie klienta
    driver.findElement(By.cssSelector("#div_1_1_3_1_1_1_1 > div > button")).click();
    System.out.println("Wyszukało");

//    Radio button z wynikiem wyszukiwania
    WebElement radioButtonClick;
    WebElement radioButtonVisibility;

    radioButtonClick = driver.findElements(By.className("dataTableSelectControl")).get(2);
    radioButtonVisibility = driver.findElement(By.cssSelector("#div_4_1_1_1_1_1_8_1_r16 > div > button"));
    wait.until(ExpectedConditions.visibilityOf(radioButtonVisibility));
    Thread.sleep(2000);
    radioButtonClick.click();
//    radioButtonClick.click();

//    ******************************Kod związany z dojśćiem do dynamicznego div'a
//    radioButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#selectionData_Table_Inner1511777670071886")));
//    radioButton.click();
//    radioButton = driver.findElements(By.className("dataTableSelectControl")).get(2);
//    radioButton = driver.findElement(By.xpath("//input[contains(@id, 'selectionData_Table_Inner')]"));
//    radioButton = driver.findElement(By.cssSelector("*[id^='selectionData_Table_Inner']"));
//
//    System.out.println(radioButton.getLocation());
//    System.out.println(radioButton.getText());
//    System.out.println(radioButton.getSize());
//
//    System.out.println(radioButton.getTagName());
//    radioButton.getAttribute("id");
//
//
//    System.out.println(radioButton.getTagName());
//    List<WebElement> asd = driver.findElements(By.xpath("//input[contains(@id, 'selectionData_Table_Inner-')]"));
//    for (WebElement we: asd
//         ) {
//      System.out.println(we);
//    }
//    System.out.println(radioButton.getText());
//    System.out.println(radioButton.getLocation());
//    System.out.println(radioButton.isSelected());
//    System.out.println(radioButton.getAttribute("class"));

//    if (!radioButton.isSelected()) {
//      radioButton.click();
//    }

//    Przejście dalej
    WebElement nextStep;

    nextStep = driver.findElement(By.cssSelector("#div_14_3_1_1_1_1_2_1_1 > div > button"));
    nextStep.click();
    Thread.sleep(4000);
    String currentURL = driver.getCurrentUrl();
    System.out.println(currentURL); // zwraca URL do kolejnego etapu procesu.

    System.out.println("Koniec Etapu 1");

    driver.switchTo().defaultContent();

//    ***************************************************************ETAP II************************************************

    Thread.sleep(10500); // <--- TODO zrobić tak, żeby czekało na załadowanie iframe w całości razem z umowami.
    WebElement frame = driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)"));
    driver.switchTo().frame(frame);

//        Wypełnienie formularza
    Select sel = new Select(driver.findElement(By.cssSelector("#combo_div_41")));
    System.out.println(sel.getOptions());
    sel.selectByVisibleText("Usterka");
//        sel.selectByIndex(1); - też UST
//        driver.findElement(By.cssSelector("option[value=\"Usterka\"]")).click();

//        new Select(driver.findElement(By.id("combo_div_2"))).selectByVisibleText("Usterka");
//        driver.findElement(By.cssSelector("option[value=\"Usterka\"]")).click();
//        new Select(driver.findElement(By.cssSelector("#combo_div_41"))).selectByVisibleText("Usterka");
    System.out.println("działa");

//        Dalej
    driver.findElement(By.cssSelector("#div_42_3_1_1_1_1_3_1_1 > div > button")).click();

    System.out.println("Koniec drugiego etapu");

    driver.switchTo().defaultContent();

//    *********************************************************ETAP III****************************************************

//    iframe
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)")));
    driver.switchTo().frame(driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)")));
    driver.manage().window().maximize();

//    Formularz
    WebElement smsNotification;
    WebElement contactPerson;
    WebElement applicationDescription;
    WebElement isAppointment;

    List<WebElement> lwe = driver.findElements(By.className("dataTableSelectControl"));

//        Sekcja klasyfikacja sprawy
    smsNotification = driver.findElement(By.cssSelector("#div_7 > div > div.controls > label > input[type=\"checkbox\"]"));
    if (smsNotification.isSelected()) {
      smsNotification.click();
    }

    contactPerson = driver.findElement(By.cssSelector("#input_div_6_1_1_1_1_1_1_3"));
    contactPerson.sendKeys("test");

    applicationDescription = driver.findElement(By.id("textArea_div_13"));
    applicationDescription.sendKeys("test");

    Select impactChannel = new Select(driver.findElement(By.cssSelector("#combo_div_8")));
    System.out.println(impactChannel.getOptions());
    impactChannel.selectByVisibleText("Osobiście");

    isAppointment = driver.findElement(By.cssSelector("#div_11 > div > div.controls > label > input[type=\"checkbox\"]"));
    if (isAppointment.isSelected()) {
      isAppointment.click();
    }

//        Umowa INO10239288
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#div_62_2_1_1_1_1_1_1_1 > div:nth-child(1)"))); // może być niepotrzebne ze względu na wcześniej wypełnianą sekcję
    System.out.println(driver.findElements(By.className("dataTableSelectControl")).size());

    String rbId = lwe.get(3).getAttribute("id");
    System.out.println(rbId);
    String rbId2 = rbId.replace("select_", "selection"); // zwraca id z innym początkiem stąd zamiana
    System.out.println(rbId2);
    Thread.sleep(500);
    driver.findElement(By.id(rbId2)).click();

//        HBO MAX HD
    Thread.sleep(2000);
    String hboFieldCheck = lwe.get(5).getAttribute("id"); // TODO sprawdzić czemu czasami jest 8 a nie 34 (size)
    System.out.println(hboFieldCheck);
    String hboFieldCheck2 = hboFieldCheck.replace("select_", "selection");
    System.out.println(hboFieldCheck2);
    Thread.sleep(100);
    driver.findElement(By.id(hboFieldCheck2)).click();

//        Sekcja decyzja (Utwórz sprawę)
    Select decision = new Select(driver.findElement(By.cssSelector("#combo_div_59")));
    System.out.println(decision.getOptions());
    decision.selectByVisibleText("Utwórz sprawę");

//        Komentarz
    WebElement insertComment;
    insertComment = driver.findElement(By.cssSelector("#textArea_div_60"));
    insertComment.sendKeys("Testowy 01");

//        Dalej
    WebElement sendApplication;
    sendApplication = driver.findElement(By.cssSelector("#div_62_3_1_1_1_1_3_1_1_1 > div:nth-child(1) > button:nth-child(1)"));
    sendApplication.click();

//        Alert
    driver.findElement(By.cssSelector("#div_61 > div:nth-child(2) > div:nth-child(3) > a:nth-child(2)")).click(); // trochę trzeba poczekać

    System.out.println("poszło");

    driver.switchTo().defaultContent();

//    *******************************************************************ETAP IV*************************************************

    //        iframe
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)")));
    driver.switchTo().frame(driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_task_form_CoachRenderer_0 > iframe:nth-child(1)"))).manage().window().maximize();
//        driver.manage().window().maximize();

//        Ankieta/wybierz zestaw
    WebElement choosePackage;
    WebElement packageTextBox;
    WebElement nextButton1;
    WebElement nextButton2;
    WebElement nextButton3;
    WebElement nextButton4;
    WebElement nextButton5;
    WebElement nextButton6;
    WebElement radioButton1;
    WebElement radioButton2;
    WebElement radioButton3;
    WebElement radioButton4;
    WebElement radioButton5;
    WebElement radioButton6;
    WebElement commectField;
    WebElement nextButtonn;

    // trzeba będzie tu poczekać
    choosePackage = driver.findElement(By.cssSelector("#div_62_1_1 > div > div.defectQuestionnairePackage > div > div > div > div > button"));
    choosePackage.click();

    packageTextBox = driver.findElement(By.cssSelector(".show-tick > div:nth-child(2) > div:nth-child(1) > input:nth-child(1)"));
    packageTextBox.sendKeys("hbo");
    packageTextBox.sendKeys(Keys.ENTER);

    radioButton1 = driver.findElements(By.name("243837_answer")).get(1);
    radioButton1.click();

    nextButton1 = driver.findElement(By.cssSelector("button.btn:nth-child(5)"));
    nextButton1.click();

    radioButton2 = driver.findElements(By.name("243839_answer")).get(1);
    radioButton2.click();

    nextButton2 = driver.findElement(By.cssSelector("button.btn:nth-child(6)"));
    nextButton2.click();

    radioButton3 = driver.findElements(By.name("243840_answer")).get(1);
    radioButton3.click();

    nextButton3 = driver.findElement(By.cssSelector("div.defectQuestionnaireQuestion:nth-child(5) > button:nth-child(6)"));
    nextButton3.click();

    radioButton4 = driver.findElements(By.name("243843_answer")).get(1);
    radioButton4.click();

    nextButton4 = driver.findElement(By.cssSelector("#div_62_1_1 > div > div.defectQuestionnaireQuestions > div:nth-child(7) > button:nth-child(6)"));
    nextButton4.click();

    radioButton5 = driver.findElements(By.name("243845_answer")).get(1);
    radioButton5.click();

    nextButton5 = driver.findElement(By.cssSelector("#div_62_1_1 > div > div.defectQuestionnaireQuestions > div:nth-child(9) > button:nth-child(6)"));
    nextButton5.click();

    radioButton6 = driver.findElement(By.name("243842_answer"));
    radioButton6.click();

    commectField = driver.findElement(By.cssSelector("textarea.form-control"));
    commectField.sendKeys("Test selenium");

    nextButton6 = driver.findElement(By.cssSelector("#div_62_1_1 > div > div.defectQuestionnaireQuestions > div:nth-child(11) > button:nth-child(6)"));
    nextButton6.click();

//    JavascriptExecutor jse = (JavascriptExecutor)driver;
//    jse.executeScript("argument[0].click", nextStep); <--- stała

//    Koniec - Etap: Wyszukiwanie i potwierdzenie abonenta
  }

  public static String getIdWithStaticPlaceholderByCssSelector(WebDriver driver, String cssPath, String staticPlaceHolder) {
    List<WebElement> list = driver.findElements(By.cssSelector(cssPath));
    String res = null;

    for (WebElement aList : list) {
      String placeholder = driver.findElement(By.xpath("//input[@placeholder='" + staticPlaceHolder + "']")).getAttribute("placeholder");
      if (aList.getAttribute("placeholder").equals(placeholder)) {
        res = aList.getAttribute("id");
        break;
      }
    }
    return res;
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
